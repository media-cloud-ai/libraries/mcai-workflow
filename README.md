# MCAI Workflow

Editor and viewer of workflow

## Demo

Go in `demo` folder and run:
```bash
trunk serve --watch ..
```

Open the result on the URL: [http://127.0.0.1:8080](http://127.0.0.1:8080)

Enjoy !
