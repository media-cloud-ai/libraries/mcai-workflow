use by_address::ByAddress;
use mcai_graph::Link;
use mcai_models::{Workflow, WorkflowDefinition, WorkflowInstance};
use mcai_workflow::{
  IconEvent, Modal, ModalMessage, SharedWorkerDefinition, SharedWorkflow, StepNameEvent,
  WorkerDefinitionEvent, WorkerDefinitionPanel, WorkerDetailsPanel, WorkflowGraph,
  WorkflowGraphEvent, WorkflowLine, WorkflowPanel, WorkflowPanelEvent,
};
use std::ops::{Deref, DerefMut};
use std::sync::{Arc, Mutex};
use wasm_bindgen::{JsCast, JsValue};
use web_sys::{Blob, BlobPropertyBag, Event, HtmlElement, HtmlInputElement, Url};
use yew::{html, Component, Context, Html};

fn main() {
  wasm_logger::init(wasm_logger::Config::default());
  yew::start_app::<Demo>();
}

pub enum DemoMessage {
  OpenWorkflow(Event),
  ErrorModal(ModalMessage),
  DownloadClick,
  Loaded(Box<serde_json::Result<WorkflowDefinition>>),
  WorkerDefinition(WorkerDefinitionEvent),
  WorkflowDefinition(WorkflowGraphEvent),
  WorkflowInstance(WorkflowGraphEvent),
  WorkflowPanelEvent(WorkflowPanelEvent),
  IconListEvent(IconEvent),
  StepRenameEvent(StepNameEvent),
}

pub struct Demo {
  empty_workflow: SharedWorkflow,
  selected_definition_step_id: Option<u32>,
  selected_definition_link: Option<Link>,
  selected_instance_step_id: Option<u32>,
  selected_instance_link: Option<Link>,
  selected_step_for_icon_update: Option<u32>,
  selected_step_for_name_update: Option<u32>,
  selected_worker_definition: Option<SharedWorkerDefinition>,
  worker_definitions: Vec<SharedWorkerDefinition>,
  workflow_find_credits: SharedWorkflow,
  workflow_instance_sleeper: WorkflowInstance,
  workflow_instance_find_credits: WorkflowInstance,
  error_modal: Option<Html>,
}

impl Component for Demo {
  type Message = DemoMessage;
  type Properties = ();

  fn create(_ctx: &Context<Self>) -> Self {
    let empty_workflow = WorkflowDefinition::new("my_workflow", "My workflow");
    let empty_workflow = ByAddress(Arc::new(Mutex::new(Workflow::Definition(empty_workflow))));

    let str_workflow = include_str!("workflows/sleeper.json");
    let workflow_instance_sleeper: WorkflowInstance = serde_json::from_str(str_workflow).unwrap();

    let str_workflow = include_str!("workflows/find_credits.json");
    let workflow_instance_find_credits: WorkflowInstance =
      serde_json::from_str(str_workflow).unwrap();
    let workflow_find_credits = ByAddress(Arc::new(Mutex::new(Workflow::Instance(
      workflow_instance_find_credits.clone(),
    ))));

    let worker_definitions = [
      include_str!("worker_definition/fake_worker.json"),
      include_str!("worker_definition/sleeper.json"),
      include_str!("worker_definition/command_line_worker.json"),
      include_str!("worker_definition/rs_transfer_worker.json"),
      include_str!("worker_definition/engine_worker.json"),
      include_str!("worker_definition/probe_worker.json"),
    ]
    .iter()
    .map(|worker_definition| serde_json::from_str(worker_definition).unwrap())
    .map(|worker_definition| ByAddress(Arc::new(Mutex::new(worker_definition))))
    .collect();

    Self {
      empty_workflow,
      workflow_find_credits,
      workflow_instance_sleeper,
      workflow_instance_find_credits,
      selected_worker_definition: None,
      selected_definition_step_id: None,
      selected_definition_link: None,
      selected_instance_step_id: None,
      selected_instance_link: None,
      worker_definitions,
      selected_step_for_icon_update: None,
      selected_step_for_name_update: None,
      error_modal: None,
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      DemoMessage::OpenWorkflow(event) => {
        if let Some(input_element) = event
          .target()
          .and_then(|t| t.dyn_into::<HtmlInputElement>().ok())
        {
          if let Some(file) = input_element.files().and_then(|f| f.get(0)) {
            ctx.link().send_future(async move {
              let blob = mio_gloo_file::Blob::from(file);
              let data = mio_gloo_file::futures::read_as_text(&blob).await.unwrap();

              let workflow_definition = serde_json::from_str(&data);
              DemoMessage::Loaded(Box::new(workflow_definition))
            });
          }
        }
      }
      DemoMessage::Loaded(workflow_definition) => match Box::leak(workflow_definition) {
        Ok(workflow_definition) => {
          *self.empty_workflow.lock().unwrap() = Workflow::Definition(workflow_definition.clone())
        }
        Err(error) => {
          self.error_modal = Some(html!(
              <Modal
                event={ctx.link().callback(DemoMessage::ErrorModal)}
                height="50vh" width="19vw"
                modal_title="Import error">
                <div class="error">
                  {error.to_string()}
                </div>
              </Modal>
          ))
        }
      },
      DemoMessage::ErrorModal(_) => self.error_modal = None,
      DemoMessage::DownloadClick => {
        if let Workflow::Definition(definition) = self.empty_workflow.lock().unwrap().deref() {
          let js_value = js_sys::Array::from(&JsValue::from_str(
            &serde_json::to_string(&definition).unwrap(),
          ));

          let blob = Blob::new_with_blob_sequence_and_options(
            &js_value,
            BlobPropertyBag::new().type_("application/json"),
          )
          .unwrap();
          let url = Url::create_object_url_with_blob(&blob).unwrap();

          let window = web_sys::window().unwrap();
          let document = window.document().unwrap();
          let a = document.create_element("a").unwrap();
          a.set_attribute("href", &url).unwrap();
          a.set_attribute("style", "display: none").unwrap();

          let (major, minor, micro) = match definition {
            WorkflowDefinition::Version1_8(definition) => (
              definition.common.version_major,
              definition.common.version_minor,
              definition.common.version_micro,
            ),
            WorkflowDefinition::Version1_9(definition) => (
              definition.common.version_major,
              definition.common.version_minor,
              definition.common.version_micro,
            ),
            WorkflowDefinition::Version1_10(definition) => (
              definition.common.version_major,
              definition.common.version_minor,
              definition.common.version_micro,
            ),
            WorkflowDefinition::Version1_11(definition) => (
              definition.common.version_major,
              definition.common.version_minor,
              definition.common.version_micro,
            ),
          };

          a.set_attribute(
            "download",
            &format!(
              "workflow_{}_v{}.{}.{}.json",
              definition.identifier(),
              major,
              minor,
              micro
            ),
          )
          .unwrap();

          document.body().unwrap().append_child(&a).unwrap();
          a.dyn_into::<HtmlElement>().unwrap().click();
          Url::revoke_object_url(&url).unwrap();
        }
      }
      DemoMessage::WorkerDefinition(WorkerDefinitionEvent::WorkerDeselected) => {
        self.selected_worker_definition = None;
      }
      DemoMessage::WorkerDefinition(WorkerDefinitionEvent::WorkerSelected(worker_definition)) => {
        self.selected_worker_definition = Some(worker_definition);
        self.selected_definition_step_id = None;
      }
      DemoMessage::WorkflowDefinition(WorkflowGraphEvent::StepSelected(step_id)) => {
        self.selected_definition_step_id = step_id;
        if self.selected_definition_step_id.is_some() {
          self.selected_worker_definition = None;
          self.selected_definition_link = None;
        }
      }
      DemoMessage::WorkflowDefinition(WorkflowGraphEvent::LinkSelected(link)) => {
        self.selected_definition_link = link;
        if self.selected_definition_link.is_some() {
          self.selected_worker_definition = None;
          self.selected_definition_step_id = None;
        }
      }
      DemoMessage::WorkflowInstance(WorkflowGraphEvent::StepSelected(step_id)) => {
        self.selected_instance_step_id = step_id;
        if self.selected_instance_step_id.is_some() {
          self.selected_instance_link = None;
        }
      }
      DemoMessage::WorkflowInstance(WorkflowGraphEvent::LinkSelected(link)) => {
        self.selected_instance_link = link;
        if self.selected_instance_link.is_some() {
          self.selected_worker_definition = None;
          self.selected_instance_step_id = None;
        }
      }
      DemoMessage::WorkflowPanelEvent(WorkflowPanelEvent::SetStepIconEvent(id)) => {
        self.selected_step_for_icon_update = Some(id);
      }
      DemoMessage::WorkflowPanelEvent(WorkflowPanelEvent::RemovedLink(_link)) => {
        self.selected_instance_link = None;
      }
      DemoMessage::IconListEvent(IconEvent::SelectedIcon(icon_name)) => {
        if let Some(step_id) = self.selected_step_for_icon_update.take() {
          if let Workflow::Definition(definition) = self.empty_workflow.lock().unwrap().deref_mut()
          {
            if let Some(step) = definition.get_mut_step(step_id) {
              step.icon = mcai_models::Icon::new(Some(icon_name)).unwrap()
            };
          }
        }
      }
      DemoMessage::WorkflowPanelEvent(WorkflowPanelEvent::SetStepNameEvent(id)) => {
        self.selected_step_for_name_update = Some(id);
      }
      DemoMessage::StepRenameEvent(StepNameEvent::InputName(name)) => {
        if let Some(step_id) = self.selected_step_for_name_update.take() {
          if let Workflow::Definition(definition) = self.empty_workflow.lock().unwrap().deref_mut()
          {
            if let Some(step) = definition.get_mut_step(step_id) {
              step.name = name;
            };
          }
        }
      }
    }
    true
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let panel = self
      .selected_worker_definition
      .as_ref()
      .map(|worker_definition| html!(
        <WorkerDetailsPanel height="50vh" width="19vw" worker_definition={worker_definition.clone()} />
      ))
      .unwrap_or_else(|| html!(
        <WorkflowPanel
          workflow={self.empty_workflow.clone()}
          height="75vh" width="25%"
          step_id={self.selected_definition_step_id}
          link={self.selected_definition_link.clone()}
          event={ctx.link().callback(DemoMessage::WorkflowPanelEvent)}/>
      ));

    let error_modal = self
      .error_modal
      .as_ref()
      .map(|content| content.clone())
      .unwrap_or_else(|| html!());

    html!(
      <>
        <div>
          <span>
            <input type="file" accept="application/json"
              onchange={ctx.link().callback(DemoMessage::OpenWorkflow)}/>
          </span>
          <button onclick={ctx.link().callback(|_| DemoMessage::DownloadClick)}>{"Download"}</button>
        </div>
        <WorkerDefinitionPanel height="75vh" width="20%"
          worker_definitions={self.worker_definitions.clone()}
          events={ctx.link().callback(DemoMessage::WorkerDefinition)}
          selected_worker_definition={self.selected_worker_definition.clone()}
          />
        <WorkflowGraph height="75vh" width="55%"
          workflow={self.empty_workflow.clone()}
          selected_step={self.selected_definition_step_id}
          selected_link={self.selected_definition_link.clone()}
          events={ctx.link().callback(DemoMessage::WorkflowDefinition)}
          />
        {panel}
        {error_modal}
        <br/>
        <br/>
        <WorkflowLine workflow={self.workflow_instance_sleeper.clone()} duration={3661} />
        <br/>
        <WorkflowLine workflow={self.workflow_instance_find_credits.clone()} />
        <br/>
        <br/>
        <WorkflowGraph workflow={self.workflow_find_credits.clone()} height="75vh" width="75%" events={ctx.link().callback(DemoMessage::WorkflowInstance)} />
        <WorkflowPanel workflow={self.workflow_find_credits.clone()} height="75vh" width="25%" step_id={self.selected_instance_step_id} />
      </>
    )
  }
}
