use css_in_rust_next::Style;
use web_sys::MouseEvent;
use yew::{classes, html, Callback, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct ToggleButtonProperties {
  pub label: String,
  pub icon: Option<Html>,
  #[prop_or_default]
  pub checked: bool,
  #[prop_or_default]
  pub disabled: bool,
  pub onclick: Callback<MouseEvent>,
  #[prop_or_default]
  pub class: Option<String>,
}

pub struct ToggleButton {
  style: Style,
}

impl Component for ToggleButton {
  type Message = ();
  type Properties = ToggleButtonProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("toggle_button.css")).unwrap();
    Self { style }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let class_names = classes!(
      self.style.to_string(),
      ctx.props().class.clone().unwrap_or_default()
    );

    html!(
      <div class={class_names} onclick={ctx.props().onclick.clone()}>
        <label class="switch">
          <input type="checkbox" checked={ctx.props().checked}/>
          <span class="slider"></span>
        </label>
        <span class="label">{&ctx.props().label}</span>
      </div>
    )
  }
}
