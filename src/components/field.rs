use css_in_rust_next::Style;
use yew::{html, Children, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct FieldProperties {
  pub label: String,
  #[prop_or_default]
  pub children: Children,
}

pub struct Field {
  style: Style,
}

impl Component for Field {
  type Message = ();
  type Properties = FieldProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      r#"
      label.fieldTitle,
      span.fieldInner {
        vertical-align: bottom;
        line-height: 23px;
        padding: 3px;
      }

      label.fieldTitle {
        display: inline-block;
        width: 200px;
        overflow: hidden;
      }
      "#,
    )
    .unwrap();

    Self { style }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    html!(
      <div class={self.style.clone()}>
        <label class="fieldTitle">
          {&ctx.props().label}
        </label>
        <span class="fieldInner">
          { for ctx.props().children.iter() }
        </span>
      </div>
    )
  }
}
