use crate::{ActionButton, Button, Field, Modal, ModalMessage};
use css_in_rust_next::Style;
use mcai_models::{NotificationHook, NotificationHookCondition};
use wasm_bindgen::JsCast;
use web_sys::{Event, EventTarget, HtmlInputElement, HtmlSelectElement, InputEvent};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{plus::Plus, trash_2::Trash2};

#[derive(PartialEq, Properties)]
pub struct EditNotificationHookProperties {
  pub event: Callback<EditNotificationHookMessage>,
  pub notification_hook: Option<NotificationHook>,
}

pub enum EditNotificationHookMessage {
  Submit(NotificationHook),
  Cancel,
  Delete,
}

pub enum FieldId {
  Label,
  Endpoint,
}

pub enum InternalMessage {
  Update(FieldId, String),
  UpdateCredential(Option<String>),
  UpdateConditions(Vec<NotificationHookCondition>),
  AddCredential,
  RemoveCredential,
  Modal(ModalMessage),
}

pub struct EditNotificationHook {
  style: Style,
  label: String,
  endpoint: String,
  credentials: Option<String>,
  conditions: Vec<NotificationHookCondition>,
}

impl EditNotificationHook {
  fn is_valid(&self) -> bool {
    !self.label.is_empty() && !self.endpoint.is_empty() && !self.conditions.is_empty()
  }
}

impl Component for EditNotificationHook {
  type Message = InternalMessage;
  type Properties = EditNotificationHookProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      concat!(include_str!("edit_notification_hook.css")),
    )
    .unwrap();

    if let Some(notification_hook) = &ctx.props().notification_hook {
      Self {
        style,
        label: notification_hook.label.clone(),
        endpoint: notification_hook.endpoint.clone(),
        credentials: notification_hook.credentials.clone(),
        conditions: notification_hook.conditions.clone(),
      }
    } else {
      Self {
        style,
        label: String::new(),
        endpoint: String::new(),
        credentials: None,
        conditions: vec![],
      }
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::Update(field, value) => match field {
        FieldId::Label => self.label = value,
        FieldId::Endpoint => self.endpoint = value,
      },
      InternalMessage::UpdateCredential(value) => self.credentials = value,
      InternalMessage::AddCredential => self.credentials = Some(String::new()),
      InternalMessage::RemoveCredential => self.credentials = None,
      InternalMessage::UpdateConditions(conditions) => {
        self.conditions = conditions;
      }
      InternalMessage::Modal(message) => match message {
        ModalMessage::Submit | ModalMessage::Update => {
          let start_parameter = NotificationHook {
            label: self.label.clone(),
            endpoint: self.endpoint.clone(),
            credentials: self.credentials.clone(),
            conditions: self.conditions.clone(),
          };

          ctx
            .props()
            .event
            .emit(EditNotificationHookMessage::Submit(start_parameter));
        }
        ModalMessage::Cancel => {
          ctx.props().event.emit(EditNotificationHookMessage::Cancel);
        }
        ModalMessage::Delete => {
          ctx.props().event.emit(EditNotificationHookMessage::Delete);
        }
      },
    }
    true
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let action_buttons = if ctx.props().notification_hook.is_some() {
      vec![ActionButton::Delete, ActionButton::Update(self.is_valid())]
    } else {
      vec![ActionButton::Submit(self.is_valid())]
    };

    let callback_label = ctx.link().batch_callback(|e: InputEvent| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::Update(FieldId::Label, input.value()))
    });

    let callback_endpoint = ctx.link().batch_callback(|e: InputEvent| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::Update(FieldId::Endpoint, input.value()))
    });

    let callback_credential = ctx.link().batch_callback(|e: InputEvent| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::UpdateCredential(Some(input.value())))
    });

    let credential_inner = self.credentials.as_ref().map(|credential| html!(
      <>
        <input type="text" value={credential.clone()} oninput={callback_credential} />
        <Button label="" icon={html!(<Trash2 />)} onclick={ctx.link().callback(|_| InternalMessage::RemoveCredential)}/>
      </>
    )).unwrap_or_else(|| html!(
      <Button label="Add value" icon={html!(<Plus />)} onclick={ctx.link().callback(|_| InternalMessage::AddCredential)}/>
    ));

    let callback_conditions = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      target
        .and_then(|t| t.dyn_into::<HtmlSelectElement>().ok())
        .map(|input| {
          let list = input.selected_options();
          let mut result = vec![];

          for index in 0..list.length() {
            if let Some(element) = list.item(index) {
              match element.get_attribute("value").as_deref() {
                Some("job_completed") => result.push(NotificationHookCondition::JobCompleted),
                Some("job_error") => result.push(NotificationHookCondition::JobError),
                Some("workflow_completed") => {
                  result.push(NotificationHookCondition::WorkflowCompleted)
                }
                Some("workflow_error") => result.push(NotificationHookCondition::WorkflowError),
                _ => {}
              }
            }
          }

          result
        })
        .map(InternalMessage::UpdateConditions)
    });

    let input_field = html!(
      <>
        <Field label="Label"><input type="text" value={self.label.clone()} oninput={callback_label} /></Field>
        <Field label="Endpoint"><input type="text" value={self.endpoint.clone()} oninput={callback_endpoint} /></Field>
        <Field label="Credentials">{credential_inner}</Field>
        <Field label="Condition(s)">
          <select onchange={callback_conditions} multiple=true>
            <option value={"job_completed"} selected={self.conditions.contains(&NotificationHookCondition::JobCompleted)}>{"Job completed"}</option>
            <option value={"job_error"} selected={self.conditions.contains(&NotificationHookCondition::JobError)}>{"Job error"}</option>
            <option value={"workflow_completed"} selected={self.conditions.contains(&NotificationHookCondition::WorkflowCompleted)}>{"Workflow completed"}</option>
            <option value={"workflow_error"} selected={self.conditions.contains(&NotificationHookCondition::WorkflowError)}>{"Workflow error"}</option>
          </select>
        </Field>
      </>
    );

    html!(
      <Modal
        event={ctx.link().callback(InternalMessage::Modal)}
        height="50vh" width="19vw"
        modal_title={"Edit Notification Hook"}
        actions={action_buttons}>
        <div class={self.style.clone()}>
          <div class="editNotificationHook">
            {input_field}
          </div>
        </div>
      </Modal>
    )
  }
}
