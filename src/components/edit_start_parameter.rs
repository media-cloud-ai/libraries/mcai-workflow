use crate::{
  components::modal::ActionButton, Button, ChoiceItem, ChoiceItemMessage, Modal, ModalMessage,
};
use css_in_rust_next::Style;
use mcai_models::{ListItem, StartParameter, StartParameterType};
use std::str::FromStr;
use wasm_bindgen::JsCast;
use web_sys::{Event, EventTarget, HtmlInputElement, HtmlSelectElement, InputEvent};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::plus::Plus;

#[derive(PartialEq, Properties)]
pub struct EditStartParameterProperties {
  pub event: Callback<EditStartParameterMessage>,
  pub start_parameter: Option<StartParameter>,
}

pub enum EditStartParameterMessage {
  Submit(StartParameter),
  Cancel,
  Delete,
}

pub enum Field {
  Accept,
  Label,
  Id,
  Kind,
  Step,
  WorkDir,
  ListItemLabel,
  ListItemId,
}

pub enum InternalMessage {
  Update(Field, String),
  UpdateRequired(bool),
  UpdateChoice(usize, String, String),
  Modal(ModalMessage),
  AddChoice,
  DeleteChoice(usize),
}

pub struct EditStartParameter {
  style: Style,
  label: String,
  id: String,
  kind: Option<StartParameterType>,
  required: bool,
  step: Option<f32>,
  work_dir: Option<String>,
  accept: Option<String>,
  list_items: Vec<ListItem>,
}

impl EditStartParameter {
  fn is_valid(&self) -> bool {
    !self.id.is_empty()
      && !self.label.is_empty()
      && self.kind.is_some()
      && !self
        .list_items
        .iter()
        .any(|item| item.id.is_empty() || item.label.is_empty())
  }
}

impl Component for EditStartParameter {
  type Message = InternalMessage;
  type Properties = EditStartParameterProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      concat!(include_str!("edit_start_parameter.css")),
    )
    .unwrap();

    if let Some(start_parameter) = &ctx.props().start_parameter {
      EditStartParameter {
        style,
        label: start_parameter.label.clone(),
        id: start_parameter.id.clone(),
        kind: Some(start_parameter.kind.clone()),
        required: start_parameter.required,
        step: start_parameter.step,
        work_dir: start_parameter.work_dir.clone(),
        accept: start_parameter.accept.clone(),
        list_items: start_parameter.items.clone(),
      }
    } else {
      EditStartParameter {
        style,
        label: String::new(),
        id: String::new(),
        kind: None,
        required: false,
        step: None,
        work_dir: None,
        accept: None,
        list_items: vec![],
      }
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::Update(field, value) => match field {
        Field::Label => self.label = value,
        Field::Id => self.id = value,
        Field::Kind => {
          self.kind = match value.as_str() {
            "file" => Some(StartParameterType::File),
            "choice" => Some(StartParameterType::Choice),
            "string" => Some(StartParameterType::String),
            "number" => Some(StartParameterType::Number),
            _ => None,
          };
          self.accept = None;
          self.step = None;
          self.list_items = vec![];
        }
        Field::Step => self.step = Some(f32::from_str(value.as_str()).unwrap_or_default()),
        Field::WorkDir => self.work_dir = Some(value),
        Field::Accept => self.accept = Some(value),
        Field::ListItemId => {}
        Field::ListItemLabel => {}
      },
      InternalMessage::UpdateRequired(required) => self.required = required,
      InternalMessage::Modal(message) => match message {
        ModalMessage::Submit | ModalMessage::Update => {
          let start_parameter = StartParameter {
            accept: self.accept.clone(),
            id: self.id.clone(),
            label: self.label.clone(),
            kind: self.kind.clone().unwrap(),
            default: None,
            value: None,
            required: self.required,
            icon: None,
            step: self.step,
            work_dir: self.work_dir.clone(),
            items: self.list_items.clone(),
          };

          ctx
            .props()
            .event
            .emit(EditStartParameterMessage::Submit(start_parameter));
        }
        ModalMessage::Cancel => {
          ctx.props().event.emit(EditStartParameterMessage::Cancel);
        }
        ModalMessage::Delete => {
          ctx.props().event.emit(EditStartParameterMessage::Delete);
        }
      },
      InternalMessage::UpdateChoice(index, id, label) => {
        if let Some(item) = self.list_items.get_mut(index) {
          item.id = id;
          item.label = label;
        }
      }
      InternalMessage::AddChoice => self.list_items.push(ListItem {
        id: String::new(),
        label: String::new(),
      }),
      InternalMessage::DeleteChoice(index) => {
        self.list_items.remove(index);
      }
    }
    true
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let action_buttons = if ctx.props().start_parameter.is_some() {
      vec![ActionButton::Delete, ActionButton::Update(self.is_valid())]
    } else {
      vec![ActionButton::Submit(self.is_valid())]
    };

    let callback_accept_extra = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::Update(Field::WorkDir, input.value()))
    });

    let callback_step_extra = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::Update(Field::Step, input.value()))
    });

    let extra_field = {
      if let Some(kind) = &self.kind {
        match kind {
          StartParameterType::Choice => {
            let choice_items: Html = self.list_items.iter().enumerate().map(|(index, item)|
              html!(
                <ChoiceItem id={item.id.clone()} label={item.label.clone()} event={ctx.link().callback(move |message| match message {
                  ChoiceItemMessage::Update(id, label) => InternalMessage::UpdateChoice(index, id, label),
                  ChoiceItemMessage::Delete => InternalMessage::DeleteChoice(index),
                })}/>
              )
            ).collect();

            html!(
              <>
                <label>
                  {"List of available choices"}
                </label>
                { choice_items }
                <div>
                  <Button
                    label="Add new choice value"
                    icon={html!(<Plus />)}
                    onclick={ctx.link().callback(|_| InternalMessage::AddChoice)}
                    />
                </div>
              </>
            )
          }
          StartParameterType::File => {
            html!(
              <>
              <div class="field">
                <label>
                  {"Accept"}
                </label>
                <span>
                  <input type="text" value={self.accept.clone()} onchange={callback_accept_extra} />
                </span>
              </div>
              </>
            )
          }
          StartParameterType::Number => {
            html!(
              <>
              <div class="field">
                <label>
                  {"Step of the float value"}
                </label>
                <span>
                  <input type="number"
                    value={self.step.map(|step| step.to_string())}
                    onchange={callback_step_extra}
                    />
                </span>
              </div>
              </>
            )
          }
          _ => {
            html!()
          }
        }
      } else {
        html!()
      }
    };

    let callback_id = ctx.link().batch_callback(|e: InputEvent| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::Update(Field::Id, input.value()))
    });

    let callback_kind = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlSelectElement>().ok());
      input.map(|input| InternalMessage::Update(Field::Kind, input.value()))
    });

    let callback_label = ctx.link().batch_callback(|e: InputEvent| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::Update(Field::Label, input.value()))
    });

    let callback_required = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::UpdateRequired(input.checked()))
    });

    let callback_work_dir = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::Update(Field::WorkDir, input.value()))
    });

    let input_field = html!(
      <>
      <div class="field">
        <label>
          {"Id"}
        </label>
        <span>
          <input type="text" value={self.id.clone()} oninput={callback_id} />
        </span>
      </div>
      <div class="field">
        <label>
          {"Label"}
        </label>
        <span>
          <input type="text" value={self.label.clone()} oninput={callback_label} />
        </span>
      </div>
      <div class="field">
        <label>
          {"Kind"}
        </label>
        <span>
          <select onchange={callback_kind.clone()}>
            <option value="" selected={self.kind.is_none()}>{"-- Select a type --"}</option>
            <option value="file" selected={self.kind == Some(StartParameterType::File)}>{"File"}</option>
            <option value="choice" selected={self.kind == Some(StartParameterType::Choice)}>{"Choice"}</option>
            <option value="string" selected={self.kind == Some(StartParameterType::String)}>{"String"}</option>
            <option value="number" selected={self.kind == Some(StartParameterType::Number)}>{"Number"}</option>
          </select>
        </span>
      </div>
      <div class="field">
        <label>
          {"Required"}
        </label>
        <span>
          <input type="checkbox" checked={self.required} onchange={callback_required} />
        </span>
      </div>
      <div class="field">
        <label>
          {"WorkDir"}
        </label>
        <span>
          <input type="text" value={self.work_dir.clone()} onchange={callback_work_dir} />
        </span>
      </div>

      {extra_field}
      </>
    );

    html!(
      <Modal
        event={ctx.link().callback(InternalMessage::Modal)}
        height="50vh" width="19vw"
        modal_title={"Edit Start Parameter"}
        actions={action_buttons}>
        <div class={self.style.clone()}>
          <div class="editInputStartParameter">
            {input_field}
          </div>
        </div>
      </Modal>
    )
  }
}
