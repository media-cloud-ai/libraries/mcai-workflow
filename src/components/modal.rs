use crate::Button;
use css_in_rust_next::Style;
use yew::{html, Callback, Children, Component, Context, Html, Properties};
use yew_feather::{chevron_right::ChevronRight, trash_2::Trash2, x::X};

#[derive(PartialEq)]
pub enum ActionButton {
  Submit(bool),
  Update(bool),
  Delete,
}

#[derive(PartialEq, Properties)]
pub struct ModalProperties {
  pub height: String,
  pub width: String,
  #[prop_or_default]
  pub children: Children,
  pub event: Callback<ModalMessage>,
  #[prop_or_default]
  pub actions: Vec<ActionButton>,
  pub modal_title: String,
}

pub enum ModalMessage {
  Cancel,
  Submit,
  Update,
  Delete,
}

pub struct Modal {
  style: Style,
}

impl Component for Modal {
  type Message = ModalMessage;
  type Properties = ModalProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("modal.css")).unwrap();
    Modal { style }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    ctx.props().event.emit(msg);
    true
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let buttons: Html = ctx
      .props()
      .actions
      .iter()
      .map(|action| match action {
        ActionButton::Submit(enabled) => html!(
          <Button
            label="Submit"
            icon={html!(<ChevronRight/>)}
            disabled={!*enabled}
            onclick={ctx.link().callback(|_|ModalMessage::Submit)}
            />
        ),
        ActionButton::Update(enabled) => html!(
          <Button
            label="Update"
            icon={html!(<ChevronRight/>)}
            disabled={!*enabled}
            onclick={ctx.link().callback(|_|ModalMessage::Update)}
            />
        ),
        ActionButton::Delete => html!(
          <Button
            label="Delete"
            icon={html!(<Trash2/>)}
            onclick={ctx.link().callback(|_|ModalMessage::Delete)}
            />
        ),
      })
      .collect();

    html!(
      <div class={self.style.clone()}>
        <div class="inner">
          <div class="title">
            <span> {ctx.props().modal_title.clone()} </span>
            <span class="close" onclick={ctx.link().callback(|_|ModalMessage::Cancel)} > <X/> </span>
          </div>
          <div class="modalContent">
            { for ctx.props().children.iter() }
          </div>
          <div class="actions">
            <Button
              label="Cancel"
              icon={html!(<X/>)}
              onclick={ctx.link().callback(|_|ModalMessage::Cancel)}
              />
            {buttons}
          </div>
        </div>
      </div>
    )
  }
}
