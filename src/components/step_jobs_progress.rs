use css_in_rust_next::Style;
use mcai_models::JobsStatus;
use yew::{html, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct StepJobsProgressProperties {
  pub title: Option<String>,
  pub expanded: bool,
  pub jobs_status: JobsStatus,
}

pub struct StepJobsProgress {
  style: Style,
}

impl Component for StepJobsProgress {
  type Message = ();
  type Properties = StepJobsProgressProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("step_jobs_progress.css")).unwrap();

    StepJobsProgress { style }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let classes = ctx.props().expanded.then(|| "extended").unwrap_or_default();
    let inner = ctx
      .props()
      .expanded
      .then(|| ctx.props().title.clone().unwrap_or_default())
      .unwrap_or_default();

    html!(
      <slot class={self.style.clone()}>
        <span class={format!("step {}", classes)}>
          <span class="progress completed" style={format!("flex: {}", ctx.props().jobs_status.completed)}></span>
          <span class="progress processing" style={format!("flex: {}", ctx.props().jobs_status.processing)}></span>
          <span class="progress queued" style={format!("flex: {}", ctx.props().jobs_status.queued)}></span>
          <span class="progress stopped" style={format!("flex: {}", ctx.props().jobs_status.stopped)}></span>
          <span class="progress skipped" style={format!("flex: {}", ctx.props().jobs_status.skipped)}></span>
          <span class="progress error" style={format!("flex: {}", ctx.props().jobs_status.errors)}></span>
          <span class="title">{inner}</span>
        </span>
      </slot>
    )
  }
}
