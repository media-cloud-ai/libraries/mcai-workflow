use css_in_rust_next::Style;
use web_sys::MouseEvent;
use yew::{classes, html, Callback, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct ButtonProperties {
  pub label: String,
  pub icon: Option<Html>,
  #[prop_or_default]
  pub disabled: bool,
  pub onclick: Callback<MouseEvent>,
  #[prop_or_default]
  pub class: Option<String>,
}

pub struct Button {
  style: Style,
}

impl Component for Button {
  type Message = ();
  type Properties = ButtonProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      r#"
      padding: 7px;
      cursor: pointer;

      svg {
        vertical-align: middle;
        height: 18px;
      }
      "#,
    )
    .unwrap();

    Button { style }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let class_names = classes!(
      self.style.to_string(),
      ctx.props().class.clone().unwrap_or_default()
    );
    html!(
      <button onclick={ctx.props().onclick.clone()} class={class_names} disabled={ctx.props().disabled}>
        {ctx.props().icon.clone().unwrap_or_default()}
        {&ctx.props().label}
      </button>
    )
  }
}
