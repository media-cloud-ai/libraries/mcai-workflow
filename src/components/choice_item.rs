use crate::Button;
use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlInputElement, InputEvent};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::trash_2::Trash2;

#[derive(PartialEq, Properties)]
pub struct ChoiceItemProperties {
  pub id: String,
  pub label: String,
  pub event: Callback<ChoiceItemMessage>,
}

pub enum ChoiceItemMessage {
  Update(String, String),
  Delete,
}

pub enum InternalMessage {
  UpdateId(String),
  UpdateLabel(String),
  Delete,
}

pub struct ChoiceItem {
  id: String,
  label: String,
}

impl Component for ChoiceItem {
  type Message = InternalMessage;
  type Properties = ChoiceItemProperties;

  fn create(ctx: &Context<Self>) -> Self {
    ChoiceItem {
      id: ctx.props().id.clone(),
      label: ctx.props().label.clone(),
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::UpdateId(value) => self.id = value,
      InternalMessage::UpdateLabel(value) => self.label = value,
      InternalMessage::Delete => {
        ctx.props().event.emit(ChoiceItemMessage::Delete);
        return false;
      }
    }

    if !self.id.is_empty() && !self.label.is_empty() {
      ctx.props().event.emit(ChoiceItemMessage::Update(
        self.id.clone(),
        self.label.clone(),
      ))
    }
    false
  }

  fn changed(&mut self, ctx: &Context<Self>) -> bool {
    self.id = ctx.props().id.clone();
    self.label = ctx.props().label.clone();
    true
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let callback_list_item_id_extra = ctx.link().batch_callback(|e: InputEvent| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::UpdateId(input.value()))
    });

    let callback_list_item_label_extra = ctx.link().batch_callback(|e: InputEvent| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::UpdateLabel(input.value()))
    });

    html!(
      <>
      <div class="field">
        <span>
          <input type="text" placeholder="Identifier" value={self.id.clone()} oninput={callback_list_item_id_extra} />
        </span>
        <span>
          <input type="text" placeholder="Label" value={self.label.clone()} oninput={callback_list_item_label_extra} />
        </span>
        <span>
          <Button
            label=""
            icon={html!(<Trash2 />)}
            onclick={ctx.link().callback(|_| InternalMessage::Delete)}
            />
        </span>
      </div>
      </>
    )
  }
}
