use crate::{ActionButton, Button, Modal, ModalMessage};
use css_in_rust_next::Style;
use std::str::FromStr;
use wasm_bindgen::JsCast;
use web_sys::{Event, EventTarget, HtmlInputElement};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{plus::Plus, trash_2::Trash2};

#[derive(PartialEq, Properties)]
pub struct EditNumberProperties {
  pub title: String,
  pub field_name: String,
  pub event: Callback<EditNumberMessage>,
  pub value: Option<f64>,
  pub number_step: f64,
  pub required: bool,
}

pub enum EditNumberMessage {
  Submit(Option<f64>),
  Cancel,
}

pub enum InternalMessage {
  Update(String),
  Modal(ModalMessage),
  AddValue,
  RemoveValue,
}

pub struct EditNumber {
  style: Style,
  value: Option<f64>,
}

impl Component for EditNumber {
  type Message = InternalMessage;
  type Properties = EditNumberProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      concat!(
        include_str!("edit_style.css"),
        include_str!("edit_number.css")
      ),
    )
    .unwrap();
    let value = ctx.props().value;
    Self { style, value }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::Update(value) => {
        self.value = Some(f64::from_str(value.as_str()).unwrap_or_default());
        false
      }
      InternalMessage::Modal(message) => {
        let event = match message {
          ModalMessage::Submit | ModalMessage::Update => {
            Some(EditNumberMessage::Submit(self.value))
          }
          ModalMessage::Cancel => Some(EditNumberMessage::Cancel),
          ModalMessage::Delete => None,
        };

        if let Some(event) = event {
          ctx.props().event.emit(event)
        }
        false
      }
      InternalMessage::AddValue => {
        self.value = Some(0f64);
        true
      }
      InternalMessage::RemoveValue => {
        self.value = None;
        true
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let input_number_callback = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::Update(input.value()))
    });

    let action_buttons = vec![ActionButton::Submit(true)];

    let inner_modal: Html = if ctx.props().required {
      html!(
        <input
          type="number"
          onchange={input_number_callback}
          value={self.value.unwrap_or_default().to_string()}
          />
      )
    } else if let Some(value) = self.value {
      html!(
        <>
          <div>
            <input type="number" step={ctx.props().number_step.to_string()} onchange={input_number_callback} value={value.to_string()}/>
          </div>
          <Button
            label="Remove value"
            icon={html!(<Trash2 />)}
            onclick={ctx.link().callback(|_|InternalMessage::RemoveValue)}
            />
        </>
      )
    } else {
      html!(
        <Button
          label="Add value"
          icon={html!(<Plus />)}
          onclick={ctx.link().callback(|_|InternalMessage::AddValue)}
          />
      )
    };

    html!(
      <Modal
        event={ctx.link().callback(InternalMessage::Modal)}
        height="50vh" width="19vw"
        modal_title={ctx.props().title.clone()}
        actions={action_buttons}>
        <div class={self.style.clone()}>
          <div class="editInput">
            {inner_modal}
          </div>
        </div>
      </Modal>
    )
  }
}
