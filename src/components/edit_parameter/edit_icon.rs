use crate::{IconEvent, IconList, Modal, ModalMessage};
use css_in_rust_next::Style;
use yew::{html, Callback, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct EditIconProperties {
  pub event: Callback<EditIconMessage>,
  pub title: String,
}

pub enum EditIconMessage {
  Selected(IconEvent),
  Cancel,
}

pub enum InternalMessage {
  Selected(IconEvent),
  Modal(ModalMessage),
}

pub struct EditIcon {
  style: Style,
}

impl Component for EditIcon {
  type Message = InternalMessage;
  type Properties = EditIconProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("edit_style.css")).unwrap();
    EditIcon { style }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::Selected(icon_event) => ctx
        .props()
        .event
        .emit(EditIconMessage::Selected(icon_event)),

      InternalMessage::Modal(ModalMessage::Cancel) => {
        ctx.props().event.emit(EditIconMessage::Cancel)
      }
      _ => {}
    }
    false
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    html!(
      <Modal
        event={ctx.link().callback(InternalMessage::Modal)}
        height="50vh" width="19vw"
        modal_title={ctx.props().title.clone()}>
        <div class={self.style.clone()}>
          <IconList event={ctx.link().callback(InternalMessage::Selected)}/>
        </div>
      </Modal>
    )
  }
}
