use crate::{ActionButton, Button, Modal, ModalMessage};
use css_in_rust_next::Style;
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{plus::Plus, trash_2::Trash2};

#[derive(PartialEq, Properties)]
pub struct EditBoolProperties {
  pub title: String,
  pub field_name: String,
  pub event: Callback<EditBoolMessage>,
  pub value: Option<bool>,
  pub required: bool,
}

pub enum EditBoolMessage {
  Submit(Option<bool>),
  Cancel,
}

pub enum InternalMessage {
  Toggle,
  Modal(ModalMessage),
  AddValue,
  RemoveValue,
}

pub struct EditBool {
  style: Style,
  value: Option<bool>,
}

impl Component for EditBool {
  type Message = InternalMessage;
  type Properties = EditBoolProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      concat!(
        include_str!("edit_style.css"),
        include_str!("edit_bool.css")
      ),
    )
    .unwrap();

    let value = ctx.props().value;
    EditBool { style, value }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::Toggle => {
        if let Some(value) = &mut self.value {
          *value = !*value;
        } else {
          self.value = Some(true);
        }
        true
      }
      InternalMessage::Modal(message) => {
        let event = match message {
          ModalMessage::Submit | ModalMessage::Update => Some(EditBoolMessage::Submit(self.value)),
          ModalMessage::Cancel => Some(EditBoolMessage::Cancel),
          ModalMessage::Delete => None,
        };

        if let Some(event) = event {
          ctx.props().event.emit(event)
        }
        false
      }
      InternalMessage::AddValue => {
        self.value = Some(true);
        true
      }
      InternalMessage::RemoveValue => {
        self.value = None;
        true
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let input_bool_callback = ctx.link().callback(|_| InternalMessage::Toggle);

    let action_buttons = vec![ActionButton::Submit(true)];

    let inner_modal: Html = if ctx.props().required {
      html! (<input type="checkbox" checked={self.value.unwrap_or_default()} onchange={input_bool_callback} />)
    } else if let Some(value) = self.value {
      html!(
        <>
        <div>
          <input type="checkbox" checked={value} onchange={input_bool_callback} />
        </div>
        <Button
          label="Remove value"
          icon={html!(<Trash2 />)}
          onclick={ctx.link().callback(|_|InternalMessage::RemoveValue)}
          />
        </>
      )
    } else {
      html!(
        <Button
          label="Add value"
          icon={html!(<Plus />)}
          onclick={ctx.link().callback(|_|InternalMessage::AddValue)}
          />
      )
    };

    html!(
      <Modal
        event={ctx.link().callback(InternalMessage::Modal)}
        height="50vh" width="19vw"
        modal_title={ctx.props().title.clone()}
        actions={action_buttons}>
        <div class={self.style.clone()}>
          <div class="editInput">
            {inner_modal}
          </div>
        </div>
      </Modal>
    )
  }
}
