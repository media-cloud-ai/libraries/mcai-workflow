use crate::{
  EditBool, EditBoolMessage, EditExtended, EditExtendedMessage, EditIcon, EditIconMessage,
  EditNumber, EditNumberMessage, EditRequirement, EditRequirementMessage, EditString,
  EditStringArray, EditStringArrayMessage, EditStringMessage, IconEvent, SharedWorkflow,
};
use css_in_rust_next::Style;
use mcai_models::{ParameterType, Workflow};
use serde_json::{Map, Value};
use std::ops::DerefMut;
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{check_square::CheckSquare, edit::Edit, slash::Slash, square::Square};

pub enum McaiFieldMessage {
  ModalDisplay,
  EditArrayString(EditStringArrayMessage),
  EditBool(EditBoolMessage),
  EditExtended(EditExtendedMessage),
  EditNumber(EditNumberMessage),
  EditRequirement(EditRequirementMessage),
  EditString(EditStringMessage),
  IconListEvent(EditIconMessage),
}

#[derive(PartialEq, Properties)]
pub struct McaiFieldProperties {
  pub kind: ParameterType,
  pub step_id: Option<u32>,
  pub field_name: String,
  #[prop_or_default]
  pub is_icon: bool,
  pub workflow: SharedWorkflow,
  pub event: Callback<()>,
}

pub struct McaiField {
  modal_display: bool,
  is_editable: bool,
  style: Style,
}

impl Component for McaiField {
  type Message = McaiFieldMessage;
  type Properties = McaiFieldProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let is_editable = ctx.props().workflow.lock().unwrap().is_definition();
    let style = Style::create("Component", include_str!("mcai_field.css")).unwrap();

    McaiField {
      modal_display: false,
      is_editable,
      style,
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      McaiFieldMessage::ModalDisplay => {
        self.modal_display = true;
        true
      }
      McaiFieldMessage::EditString(message) => {
        if let EditStringMessage::Submit(value) = message {
          if let Workflow::Definition(definition) = ctx.props().workflow.lock().unwrap().deref_mut()
          {
            if let Some(step_id) = ctx.props().step_id {
              let is_parameter = ctx.props().field_name != "name";
              definition.set_parameter_on_step(
                step_id,
                &ctx.props().field_name,
                ParameterType::String {
                  value,
                  default: None,
                  required: true,
                },
                is_parameter,
              );
            } else {
              definition.set_workflow_property(
                &ctx.props().field_name,
                ParameterType::String {
                  value,
                  default: None,
                  required: true,
                },
              );
            }
          }
        }

        ctx.props().event.emit(());
        self.modal_display = false;
        true
      }
      McaiFieldMessage::EditExtended(message) => {
        if let EditExtendedMessage::Submit(value) = message {
          if let Workflow::Definition(definition) = ctx.props().workflow.lock().unwrap().deref_mut()
          {
            if let Some(step_id) = ctx.props().step_id {
              let is_parameter = ctx.props().field_name != "name";
              definition.set_parameter_on_step(
                step_id,
                &ctx.props().field_name,
                ParameterType::Extended {
                  value,
                  default: Value::Object(Map::default()),
                  required: true,
                },
                is_parameter,
              );
            } else {
              definition.set_workflow_property(
                &ctx.props().field_name,
                ParameterType::Extended {
                  value,
                  default: Value::Object(Map::default()),
                  required: true,
                },
              );
            }
          }
        }

        ctx.props().event.emit(());
        self.modal_display = false;
        true
      }
      McaiFieldMessage::IconListEvent(message) => {
        if let EditIconMessage::Selected(value) = message {
          if let Workflow::Definition(definition) = ctx.props().workflow.lock().unwrap().deref_mut()
          {
            let IconEvent::SelectedIcon(icon_name) = value;
            if let Some(step_id) = ctx.props().step_id {
              definition.set_parameter_on_step(
                step_id,
                &ctx.props().field_name,
                ParameterType::String {
                  value: Some(icon_name),
                  default: None,
                  required: true,
                },
                false,
              );
            } else {
              definition.set_workflow_property(
                &ctx.props().field_name,
                ParameterType::String {
                  value: Some(icon_name),
                  default: None,
                  required: true,
                },
              );
            }
          }

          ctx.props().event.emit(());
        }
        self.modal_display = false;
        true
      }
      McaiFieldMessage::EditArrayString(message) => {
        if let EditStringArrayMessage::Submit(value) = message {
          if let Workflow::Definition(definition) = ctx.props().workflow.lock().unwrap().deref_mut()
          {
            if let Some(step_id) = ctx.props().step_id {
              let is_parameter = ctx.props().field_name != "name";
              definition.set_parameter_on_step(
                step_id,
                &ctx.props().field_name,
                ParameterType::ArrayOfStrings {
                  value,
                  default: vec![],
                  required: true,
                },
                is_parameter,
              );
            } else {
              definition.set_workflow_property(
                &ctx.props().field_name,
                ParameterType::ArrayOfStrings {
                  value,
                  default: vec![],
                  required: true,
                },
              );
            }
          }
        }

        ctx.props().event.emit(());
        self.modal_display = false;
        true
      }
      McaiFieldMessage::EditNumber(message) => {
        if let EditNumberMessage::Submit(value) = message {
          let new_value = match ctx.props().kind {
            ParameterType::Number { .. } => ParameterType::Number {
              default: None,
              value,
              required: true,
            },
            ParameterType::Integer { .. } => ParameterType::Integer {
              default: None,
              value: value.map(|v| v as u32),
              required: true,
            },
            _ => unreachable!(),
          };

          if let Workflow::Definition(definition) = ctx.props().workflow.lock().unwrap().deref_mut()
          {
            if let Some(step_id) = ctx.props().step_id {
              let is_parameter = ctx.props().field_name != "name";
              definition.set_parameter_on_step(
                step_id,
                &ctx.props().field_name,
                new_value,
                is_parameter,
              );
            } else {
              definition.set_workflow_property(&ctx.props().field_name, new_value);
            }
          }
        }

        ctx.props().event.emit(());
        self.modal_display = false;
        true
      }
      McaiFieldMessage::EditBool(message) => {
        if let EditBoolMessage::Submit(value) = message {
          if let Workflow::Definition(definition) = ctx.props().workflow.lock().unwrap().deref_mut()
          {
            if let Some(step_id) = ctx.props().step_id {
              let is_parameter = ctx.props().field_name != "name";
              definition.set_parameter_on_step(
                step_id,
                &ctx.props().field_name,
                ParameterType::Boolean {
                  default: None,
                  value,
                  required: true,
                },
                is_parameter,
              );
            } else {
              definition.set_workflow_property(
                &ctx.props().field_name,
                ParameterType::Boolean {
                  default: None,
                  value,
                  required: true,
                },
              );
            }
          }
        }

        ctx.props().event.emit(());
        self.modal_display = false;
        true
      }
      McaiFieldMessage::EditRequirement(message) => {
        if let EditRequirementMessage::Submit(value) = message {
          if let Workflow::Definition(definition) = ctx.props().workflow.lock().unwrap().deref_mut()
          {
            if let Some(step_id) = ctx.props().step_id {
              definition.set_parameter_on_step(
                step_id,
                &ctx.props().field_name,
                ParameterType::Requirements {
                  default: None,
                  value,
                  required: true,
                },
                true,
              );
            }
          }
        }
        ctx.props().event.emit(());
        self.modal_display = false;
        true
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let modal = self
      .modal_display
      .then(|| {
        let title = format!("Edit {}", ctx.props().field_name);

        match &ctx.props().kind {
          ParameterType::ArrayOfStrings { value, .. }
          | ParameterType::ArrayOfTemplates { value, .. } => html!(
            <EditStringArray
              field_name={ctx.props().field_name.clone()}
              event={ctx.link().callback(McaiFieldMessage::EditArrayString)}
              title={title}
              value={value.clone()}
              />
          ),
          ParameterType::Boolean {
            value, required, ..
          } => html!(
            <EditBool
              field_name={ctx.props().field_name.clone()}
              event={ctx.link().callback(McaiFieldMessage::EditBool)}
              title={title}
              value={*value}
              required={*required}
              />
          ),
          ParameterType::Integer {
            value, required, ..
          } => html!(
            <EditNumber
              field_name={ctx.props().field_name.clone()}
              event={ctx.link().callback(McaiFieldMessage::EditNumber)}
              title={title}
              value={value.map(|v| v as f64)}
              number_step={1.0}
              required={*required}
              />
          ),
          ParameterType::Number {
            value, required, ..
          } => html!(
            <EditNumber
              field_name={ctx.props().field_name.clone()}
              event={ctx.link().callback(McaiFieldMessage::EditNumber)}
              title={title}
              value={*value}
              number_step={0.00000000001}
              required={*required}
              />
          ),
          ParameterType::Requirements { value, .. } => html!(
            <EditRequirement
              field_name={ctx.props().field_name.clone()}
              event={ctx.link().callback(McaiFieldMessage::EditRequirement)}
              title={title}
              requirement={value.clone()}
              />
          ),
          ParameterType::String {
            value, required, ..
          }
          | ParameterType::Template {
            value, required, ..
          } => {
            if ctx.props().is_icon {
              html!(
                <EditIcon
                  event={ctx.link().callback(McaiFieldMessage::IconListEvent)}
                  title={title}
                  />
              )
            } else {
              html!(
                <EditString
                  event={ctx.link().callback(McaiFieldMessage::EditString)}
                  field_name={ctx.props().field_name.clone()}
                  title={title}
                  value={value.clone()}
                  required={*required}
                  />
              )
            }
          }
          ParameterType::Extended {
            value, required, ..
          } => {
            html!(
              <EditExtended
                event={ctx.link().callback(McaiFieldMessage::EditExtended)}
                field_name={ctx.props().field_name.clone()}
                title={title}
                value={value.clone()}
                required={*required}
                />
            )
          }
          _ => html!(),
        }
      })
      .unwrap_or_default();

    let display_value = {
      match &ctx.props().kind {
        ParameterType::ArrayOfStrings { value, .. }
        | ParameterType::ArrayOfTemplates { value, .. } => value
          .iter()
          .map(|value| {
            html!(
              <div>
                {value}
              </div>
            )
          })
          .collect::<Html>(),
        ParameterType::Boolean {
          value, required, ..
        } => {
          let inner = match if *required {
            Some(value.unwrap_or_default())
          } else {
            value.map(|value| value)
          } {
            Some(true) => html!(<CheckSquare />),
            Some(false) => html!(<Square />),
            None => html!(<Slash color={"#888"} />),
          };

          html!(<div>{inner}</div>)
        }
        ParameterType::Integer {
          value, required, ..
        } => value
          .map(|value| html!(value.to_string()))
          .unwrap_or_else(|| {
            if *required {
              html!(0)
            } else {
              html!(<Slash color={"#888"} />)
            }
          }),
        ParameterType::Number {
          value, required, ..
        } => value
          .map(|value| html!(value.to_string()))
          .unwrap_or_else(|| {
            if *required {
              html!(0f64)
            } else {
              html!(<Slash color={"#888"} />)
            }
          }),
        ParameterType::Requirements {
          value, required, ..
        } => value
          .as_ref()
          .map(|value| html!(<>{value.paths.len()}{" path(s)"}</>))
          .unwrap_or_else(|| {
            if *required {
              html!("")
            } else {
              html!(<Slash color={"#888"} />)
            }
          }),
        ParameterType::String {
          value, required, ..
        }
        | ParameterType::Template {
          value, required, ..
        } => {
          if ctx.props().is_icon {
            html!(<i class="material-icons">{value.clone().unwrap_or_default()}</i>)
          } else {
            value.as_ref().map(|value| html!(value)).unwrap_or_else(|| {
              (!required)
                .then(|| html!(<Slash color={"#888"} />))
                .unwrap_or_default()
            })
          }
        }
        ParameterType::Extended {
          value, required, ..
        } => {
          if matches!(value, Value::Null) {
            if *required {
              html!("")
            } else {
              html!(<Slash color={"#888"} />)
            }
          } else {
            html!(value)
          }
        }
        _ => html!(),
      }
    };

    if self.is_editable {
      html!(
        <span class={self.style.clone()} onclick={ctx.link().callback(|_|McaiFieldMessage::ModalDisplay)}>
          <span class="value">
            {display_value}
          </span>
          <Edit class="fieldIcon" />
          {modal}
        </span>
      )
    } else {
      html!(
        <span class={self.style.clone()}>
          <span class="value">
            {display_value}
          </span>
        </span>
      )
    }
  }
}
