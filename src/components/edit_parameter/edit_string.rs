use crate::{ActionButton, Button, Modal, ModalMessage};
use css_in_rust_next::Style;
use wasm_bindgen::JsCast;
use web_sys::{Event, EventTarget, HtmlInputElement};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{plus::Plus, trash_2::Trash2};

#[derive(PartialEq, Properties)]
pub struct EditStringProperties {
  pub title: String,
  pub field_name: String,
  pub event: Callback<EditStringMessage>,
  pub value: Option<String>,
  pub required: bool,
}

pub enum EditStringMessage {
  Submit(Option<String>),
  Cancel,
}

pub enum InternalMessage {
  Update(String),
  Modal(ModalMessage),
  AddValue,
  RemoveValue,
}

pub struct EditString {
  style: Style,
  value: Option<String>,
}

impl Component for EditString {
  type Message = InternalMessage;
  type Properties = EditStringProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      concat!(
        include_str!("edit_style.css"),
        include_str!("edit_string.css")
      ),
    )
    .unwrap();

    let value = ctx.props().value.clone();
    EditString { style, value }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::Update(value) => {
        self.value = Some(value);
        false
      }
      InternalMessage::Modal(message) => {
        let event = match message {
          ModalMessage::Submit | ModalMessage::Update => {
            Some(EditStringMessage::Submit(self.value.clone()))
          }
          ModalMessage::Cancel => Some(EditStringMessage::Cancel),
          ModalMessage::Delete => None,
        };

        if let Some(event) = event {
          ctx.props().event.emit(event)
        }
        false
      }
      InternalMessage::AddValue => {
        self.value = Some("".to_string());
        true
      }
      InternalMessage::RemoveValue => {
        self.value = None;
        false
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let input_string_callback = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| InternalMessage::Update(input.value()))
    });

    let action_buttons = vec![ActionButton::Submit(true)];

    let inner_modal: Html = if ctx.props().required {
      html!(
        <input onchange={input_string_callback} value={self.value.clone()} />
      )
    } else if let Some(value) = &self.value {
      html! (
        <>
          <div>
            <input onchange={input_string_callback} value={value.clone()} />
          </div>
          <div>
            <Button
              label="Remove value"
              icon={html!(<Trash2 />)}
              onclick={ctx.link().callback(|_|InternalMessage::RemoveValue)}
              />
          </div>
        </>
      )
    } else {
      html!(
        <Button
          label="Add value"
          icon={html!(<Plus />)}
          onclick={ctx.link().callback(|_|InternalMessage::AddValue)}
          />
      )
    };

    html!(
      <Modal
        event={ctx.link().callback(InternalMessage::Modal)}
        height="50vh" width="19vw"
        modal_title={ctx.props().title.clone()}
        actions={action_buttons}>
        <div class={self.style.clone()}>
          <div class="editInput">
            {inner_modal}
          </div>
        </div>
      </Modal>
    )
  }
}
