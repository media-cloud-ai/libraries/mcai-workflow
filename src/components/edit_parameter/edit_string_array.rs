use crate::{ActionButton, Button, Modal, ModalMessage};
use css_in_rust_next::Style;
use wasm_bindgen::JsCast;
use web_sys::{Event, EventTarget, HtmlInputElement};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{plus::Plus, trash_2::Trash2};

#[derive(PartialEq, Properties)]
pub struct EditStringArrayProperties {
  pub title: String,
  pub field_name: String,
  pub event: Callback<EditStringArrayMessage>,
  pub value: Vec<String>,
}

pub enum EditStringArrayMessage {
  Submit(Vec<String>),
  Cancel,
}
pub enum InternalMessage {
  Add,
  Delete(usize),
  Update((usize, String)),
  Modal(ModalMessage),
}

pub struct EditStringArray {
  style: Style,
  value: Vec<String>,
}

impl Component for EditStringArray {
  type Message = InternalMessage;
  type Properties = EditStringArrayProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      concat!(
        include_str!("edit_style.css"),
        include_str!("edit_string_array.css")
      ),
    )
    .unwrap();

    let value = ctx.props().value.clone();
    EditStringArray { style, value }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::Add => {
        self.value.push("".to_string());
        true
      }
      InternalMessage::Delete(index) => {
        self.value.remove(index);
        true
      }
      InternalMessage::Update((index, value)) => {
        self.value[index] = value;
        false
      }
      InternalMessage::Modal(message) => {
        let event = match message {
          ModalMessage::Submit | ModalMessage::Update => {
            Some(EditStringArrayMessage::Submit(self.value.clone()))
          }
          ModalMessage::Cancel => Some(EditStringArrayMessage::Cancel),
          ModalMessage::Delete => None,
        };

        if let Some(event) = event {
          ctx.props().event.emit(event)
        }
        false
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let action_buttons = vec![ActionButton::Submit(true)];

    let inner: Html = self
      .value
      .iter()
      .enumerate()
      .map(|(index, value)| {
        let cloned_index = index;

        let callback = ctx.link().batch_callback(move |e: Event| {
          let target: Option<EventTarget> = e.target();
          let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
          input.map(|input| InternalMessage::Update((cloned_index, input.value())))
        });

        html!(
          <div class="item">
            <input type="text" value={value.clone()} onchange={callback} />
            <Button
              label=""
              icon={html!(<Trash2 />)}
              onclick={ctx.link().callback(move |_| InternalMessage::Delete(index))}
              />
          </div>
        )
      })
      .collect();

    html!(
      <Modal
        event={ctx.link().callback(InternalMessage::Modal)}
        height="50vh" width="19vw"
        modal_title={ctx.props().title.clone()}
        actions={action_buttons}>
        <div class={self.style.clone()}>
          {inner}
          <div class="add">
            <Button
              label="Add another value"
              icon={html!(<Plus />)}
              onclick={ctx.link().callback(move |_| InternalMessage::Add)}
              />
          </div>
        </div>
      </Modal>
    )
  }
}
