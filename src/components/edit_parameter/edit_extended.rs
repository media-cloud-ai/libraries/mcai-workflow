use crate::{ActionButton, Button, Modal, ModalMessage};
use css_in_rust_next::Style;
use serde_json::{Map, Value};
use wasm_bindgen::JsCast;
use web_sys::{Event, EventTarget, HtmlTextAreaElement};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{plus::Plus, trash_2::Trash2};

#[derive(PartialEq, Properties)]
pub struct EditExtendedProperties {
  pub title: String,
  pub field_name: String,
  pub event: Callback<EditExtendedMessage>,
  pub value: Value,
  pub required: bool,
}

#[derive(Debug)]
pub enum EditExtendedMessage {
  Submit(Value),
  Cancel,
}

pub enum InternalMessage {
  Update(Value),
  Modal(ModalMessage),
  AddValue,
  RemoveValue,
}

pub struct EditExtended {
  style: Style,
  value: Value,
}

impl Component for EditExtended {
  type Message = InternalMessage;
  type Properties = EditExtendedProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      concat!(
        include_str!("edit_style.css"),
        include_str!("edit_extended.css")
      ),
    )
    .unwrap();

    let value = if matches!(ctx.props().value, Value::Null) {
      Value::Object(Map::default())
    } else {
      ctx.props().value.clone()
    };
    EditExtended { style, value }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::Update(value) => {
        self.value = value;
        false
      }
      InternalMessage::Modal(message) => {
        let event = match message {
          ModalMessage::Submit | ModalMessage::Update => {
            Some(EditExtendedMessage::Submit(self.value.clone()))
          }
          ModalMessage::Cancel => Some(EditExtendedMessage::Cancel),
          ModalMessage::Delete => None,
        };

        if let Some(event) = event {
          ctx.props().event.emit(event)
        }
        false
      }
      InternalMessage::AddValue => {
        self.value = Value::Object(Map::default());
        true
      }
      InternalMessage::RemoveValue => {
        self.value = Value::Null;
        false
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let input_extended_callback = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      let textarea: Option<HtmlTextAreaElement> =
        target.and_then(|t| t.dyn_into::<HtmlTextAreaElement>().ok());
      textarea.map(|textarea| {
        let textarea_value = textarea.value();
        let result = serde_json::from_str(&textarea_value);
        let json = result
          .map(Value::Object)
          .unwrap_or_else(|_error| Value::Object(Map::default()));
        InternalMessage::Update(json)
      })
    });

    let action_buttons = vec![ActionButton::Submit(true)];

    let inner_modal: Html = if ctx.props().required {
      html!(
        <textarea onchange={input_extended_callback} value={self.value.to_string()} />
      )
    } else if matches!(self.value, Value::Object(_)) {
      html! (
        <>
          <div>
            <textarea onchange={input_extended_callback} value={self.value.to_string()} />
          </div>
          <div>
            <Button
              label="Remove value"
              icon={html!(<Trash2 />)}
              onclick={ctx.link().callback(|_|InternalMessage::RemoveValue)}
              />
          </div>
        </>
      )
    } else {
      html!(
        <Button
          label="Add value"
          icon={html!(<Plus />)}
          onclick={ctx.link().callback(|_|InternalMessage::AddValue)}
          />
      )
    };

    html!(
      <Modal
        event={ctx.link().callback(InternalMessage::Modal)}
        height="50vh" width="19vw"
        modal_title={ctx.props().title.clone()}
        actions={action_buttons}>
        <div class={self.style.clone()}>
          <div class="editInput">
            {inner_modal}
          </div>
        </div>
      </Modal>
    )
  }
}
