use crate::{ActionButton, Button, Modal, ModalMessage};
use css_in_rust_next::Style;
use mcai_models::Requirement;
use wasm_bindgen::JsCast;
use web_sys::{Event, EventTarget, HtmlInputElement};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{plus::Plus, trash_2::Trash2};

#[derive(PartialEq, Properties)]
pub struct EditRequirementProperties {
  pub title: String,
  pub field_name: String,
  pub event: Callback<EditRequirementMessage>,
  pub requirement: Option<Requirement>,
}

pub enum EditRequirementMessage {
  Submit(Option<Requirement>),
  Cancel,
}
pub enum InternalMessage {
  AddPath,
  DeletePath(usize),
  UpdatePath((usize, String)),
  Modal(ModalMessage),
}

pub struct EditRequirement {
  style: Style,
  requirement: Option<Requirement>,
}

impl Component for EditRequirement {
  type Message = InternalMessage;
  type Properties = EditRequirementProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let style = Style::create(
      "Component",
      concat!(
        include_str!("edit_style.css"),
        include_str!("edit_requirements.css")
      ),
    )
    .unwrap();

    let requirement = ctx.props().requirement.clone();
    Self { style, requirement }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::AddPath => {
        if let Some(requirement) = &mut self.requirement {
          requirement.paths.push("".to_string())
        } else {
          self.requirement = Some(Requirement {
            paths: vec!["".to_string()],
          })
        }
        true
      }
      InternalMessage::DeletePath(index) => {
        if let Some(requirement) = &mut self.requirement {
          requirement.paths.remove(index);
        }
        true
      }
      InternalMessage::UpdatePath((index, value)) => {
        if let Some(requirement) = &mut self.requirement {
          requirement.paths[index] = value;
        }
        false
      }
      InternalMessage::Modal(message) => {
        let event = match message {
          ModalMessage::Submit | ModalMessage::Update => {
            Some(EditRequirementMessage::Submit(self.requirement.clone()))
          }
          ModalMessage::Cancel => Some(EditRequirementMessage::Cancel),
          ModalMessage::Delete => None,
        };

        if let Some(event) = event {
          ctx.props().event.emit(event)
        }
        false
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let action_buttons = vec![ActionButton::Submit(true)];

    let inner: Html = self
      .requirement
      .as_ref()
      .map(|requirement| {
        requirement
          .paths
          .iter()
          .enumerate()
          .map(|(index, value)| {
            let cloned_index = index;

            let callback = ctx.link().batch_callback(move |e: Event| {
              let target: Option<EventTarget> = e.target();
              let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
              input.map(|input| InternalMessage::UpdatePath((cloned_index, input.value())))
            });

            html!(
              <div class="item">
                <input type="text" value={value.clone()} onchange={callback} />
                <Button
                  label=""
                  icon={html!(<Trash2 />)}
                  onclick={ctx.link().callback(move |_| InternalMessage::DeletePath(index))}
                  />
              </div>
            )
          })
          .collect()
      })
      .unwrap_or_default();

    html!(
      <Modal
        event={ctx.link().callback(InternalMessage::Modal)}
        height="50vh" width="19vw"
        modal_title={ctx.props().title.clone()}
        actions={action_buttons}>
        <div class={self.style.clone()}>
          {"Paths"}
          {inner}
          <div class="add">
            <Button
              label="Add an another path"
              icon={html!(<Plus />)}
              onclick={ctx.link().callback(|_| InternalMessage::AddPath)}
              />
          </div>
        </div>
      </Modal>
    )
  }
}
