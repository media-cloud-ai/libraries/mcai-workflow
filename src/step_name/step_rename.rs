use css_in_rust_next::Style;
use wasm_bindgen::JsCast;
use web_sys::{Event, EventTarget, HtmlInputElement};
use yew::{html, Callback, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct StepRenameProperties {
  pub height: String,
  pub width: String,
  pub event: Option<Callback<StepNameEvent>>,
}

pub enum StepNameEvent {
  InputName(String),
}

pub enum StepRenameMessage {
  NewName(String),
  SubmitNewName,
}

pub struct StepRename {
  pub style: Style,
  pub new_name: Option<String>,
}

impl Component for StepRename {
  type Message = StepRenameMessage;
  type Properties = StepRenameProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("step_rename.css")).unwrap();
    StepRename {
      style,
      new_name: None,
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      StepRenameMessage::NewName(new_name) => {
        self.new_name = Some(new_name);
        true
      }
      StepRenameMessage::SubmitNewName => {
        if let Some(new_name) = &self.new_name {
          if let Some(callback) = ctx.props().event.as_ref() {
            callback.emit(StepNameEvent::InputName(new_name.clone()))
          }
        }
        true
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let style = format!(
      "height: {}; width: {};",
      ctx.props().height,
      ctx.props().width,
    );

    let set_step_name_callback = ctx.link().batch_callback(|e: Event| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| StepRenameMessage::NewName(input.value()))
    });

    html!(
      <span class={self.style.clone()} style={style}>
        <div class="title">
          {"Step Name: "}
        </div>
        <input
          onchange={set_step_name_callback}
        />
        <button type="button" onclick={ctx.link().callback(|_|StepRenameMessage::SubmitNewName)}>
          {"Rename"}
        </button>
      </span>
    )
  }
}
