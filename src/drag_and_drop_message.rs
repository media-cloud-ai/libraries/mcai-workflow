use mcai_models::WorkerDefinition;
use serde::{Deserialize, Serialize};

pub static MCAI_DRAG_AND_DROP_ID: &str = "MCAI";

#[derive(Debug, Deserialize, Serialize)]
pub enum DragAndDropMessage {
  Worker(WorkerDefinition),
}
