use mcai_types::Coordinates;

#[derive(Debug)]
pub enum Moving {
  View(Coordinates),
  Step(u32, Coordinates),
  Input(u32, Coordinates),
  Output(u32, Coordinates),
}
