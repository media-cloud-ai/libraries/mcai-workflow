use super::{Moving, Node, SvgLink};
use crate::{colors::*, DragAndDropMessage, ToggleButton, MCAI_DRAG_AND_DROP_ID};
use by_address::ByAddress;
use css_in_rust_next::Style;
use mcai_graph::{Graph, GraphConfiguration, Link, LinkType, NodeConfiguration, ToGraph};
use mcai_models::{Icon, Parameter, Step, StepMode, Workflow};
use mcai_types::Coordinates;
use std::{
  ops::DerefMut,
  sync::{Arc, Mutex},
};
use web_sys::{DragEvent, MouseEvent, WheelEvent};
use yew::{html, Callback, Component, Context, Html, Properties};

pub type SharedWorkflow = ByAddress<Arc<Mutex<Workflow>>>;

#[derive(PartialEq, Properties)]
pub struct WorkflowGraphProperties {
  pub workflow: SharedWorkflow,
  pub height: String,
  pub width: String,
  pub selected_step: Option<u32>,
  #[prop_or_default]
  pub selected_link: Option<Link>,
  pub events: Option<Callback<WorkflowGraphEvent>>,
}

pub enum WorkflowGraphEvent {
  StepSelected(Option<u32>),
  LinkSelected(Option<Link>),
}

#[derive(Debug, PartialEq)]
pub enum WorkflowGraphMessage {
  SelectStep(u32),
  RemoveStep(u32),
  SelectLink(Link),
  RemoveLink(u32, u32),
  DeselectMovingStep,
  SelectMovingStep(u32, Coordinates),
  SelectInputStep(u32, Coordinates),
  SelectOutputStep(u32, Coordinates),
  OnClick(MouseEvent),
  OnDragOver(DragEvent),
  OnDrop(DragEvent),
  OnMouseDown(MouseEvent),
  OnMouseMove(MouseEvent),
  OnMouseUp(MouseEvent),
  OnMouseUpStepInput(u32),
  OnMouseUpStepOutput(u32),
  OnWheel(WheelEvent),
  ToggleRequirementLinks(MouseEvent),
}

pub struct WorkflowGraph {
  style: Style,
  offset: Coordinates,
  moving: Option<Moving>,
  zoom: f32,
  configuration: GraphConfiguration,
  graph: Graph,
  drawing_link: Option<Link>,
  link_type: LinkType,
}

impl WorkflowGraph {
  fn emit(&self, ctx: &Context<Self>, event: WorkflowGraphEvent) {
    if let Some(events) = ctx.props().events.as_ref() {
      events.emit(event)
    }
  }
}

impl Component for WorkflowGraph {
  type Message = WorkflowGraphMessage;
  type Properties = WorkflowGraphProperties;

  fn create(ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("workflow.css")).unwrap();

    let configuration = GraphConfiguration::new(NodeConfiguration::new(200, 50, 30, 30));
    let graph = ctx
      .props()
      .workflow
      .lock()
      .unwrap()
      .to_graph(configuration.clone());

    ctx
      .props()
      .workflow
      .lock()
      .unwrap()
      .update_steps_coordinates_from_graph(&graph);

    let offset = Coordinates { x: 30, y: 30 };

    WorkflowGraph {
      style,
      offset,
      moving: None,
      zoom: 1.0,
      configuration,
      graph,
      drawing_link: None,
      link_type: LinkType::Parentage,
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    let is_definition = ctx.props().workflow.lock().unwrap().is_definition();

    match msg {
      WorkflowGraphMessage::SelectStep(step_id) => {
        self.emit(ctx, WorkflowGraphEvent::StepSelected(Some(step_id)));
      }
      WorkflowGraphMessage::RemoveStep(step_id) => {
        let mut workflow = ctx.props().workflow.lock().unwrap();

        if let Workflow::Definition(workflow_definition) = workflow.deref_mut() {
          let steps = workflow_definition.get_mut_steps();

          steps.retain(|step| step.id != step_id);

          steps.iter_mut().for_each(|step| {
            step.parent_ids.retain(|parent_id| *parent_id != step_id);
            step
              .required_to_start
              .retain(|required| *required != step_id);
          });
        }

        self.emit(ctx, WorkflowGraphEvent::StepSelected(None));
      }
      WorkflowGraphMessage::SelectLink(link) => {
        self.emit(ctx, WorkflowGraphEvent::LinkSelected(Some(link)));
      }
      WorkflowGraphMessage::RemoveLink(child_id, parent_id) => {
        let mut workflow = ctx.props().workflow.lock().unwrap();

        if let Workflow::Definition(workflow_definition) = workflow.deref_mut() {
          if let Some(step) = workflow_definition.get_mut_step(child_id) {
            let parents = match self.link_type {
              LinkType::Parentage => &mut step.parent_ids,
              LinkType::Requirement => &mut step.required_to_start,
            };
            parents.retain(|id| *id != parent_id);
          }
          self.graph.disconnect(parent_id, child_id, &self.link_type);
        }
      }
      WorkflowGraphMessage::SelectMovingStep(step_id, coordinates) => {
        if is_definition {
          self.moving = Some(Moving::Step(step_id, coordinates));
        }
      }
      WorkflowGraphMessage::SelectInputStep(step_id, coordinates) => {
        if is_definition {
          self.moving = Some(Moving::Input(step_id, coordinates));
        }
      }
      WorkflowGraphMessage::SelectOutputStep(step_id, coordinates) => {
        if is_definition {
          self.moving = Some(Moving::Output(step_id, coordinates));
        }
      }
      WorkflowGraphMessage::DeselectMovingStep => {
        self.moving = None;
        self.drawing_link = None;
      }
      WorkflowGraphMessage::OnMouseDown(mouse_event) => {
        if self.moving.is_none() {
          let coordinates = Coordinates {
            x: mouse_event.client_x() as isize - self.offset.x,
            y: mouse_event.client_y() as isize - self.offset.y,
          };

          self.moving = Some(Moving::View(coordinates));
        }
      }
      WorkflowGraphMessage::OnClick(_mouse_event) => {
        if ctx.props().selected_link.is_some() {
          self.emit(ctx, WorkflowGraphEvent::LinkSelected(None));
        }
        if ctx.props().selected_step.is_some() {
          self.emit(ctx, WorkflowGraphEvent::StepSelected(None));
        }
      }
      WorkflowGraphMessage::OnDragOver(event) => {
        event.prevent_default();
      }
      WorkflowGraphMessage::OnDrop(event) => {
        if let Some(data) = event.data_transfer() {
          if let Ok(data) = data.get_data(MCAI_DRAG_AND_DROP_ID) {
            let message: DragAndDropMessage = serde_json::from_str(&data).unwrap();

            match message {
              DragAndDropMessage::Worker(worker_definition) => {
                if is_definition {
                  if let Workflow::Definition(definition) =
                    ctx.props().workflow.lock().unwrap().deref_mut()
                  {
                    let steps = definition.get_mut_steps();

                    let index = steps.len() as u32;

                    let parameters: Vec<_> = worker_definition
                      .parameters
                      .schema
                      .object
                      .unwrap()
                      .properties
                      .iter()
                      .filter_map(|(key, value)| Parameter::new(key, value).ok())
                      .collect();

                    let coord = Coordinates {
                      x: event.offset_x() as isize - 30 - 100,
                      y: event.offset_y() as isize - 30 - 25,
                    };

                    steps.push(Step {
                      icon: Icon {
                        icon: Some("settings".to_string()),
                      },
                      id: index,
                      label: worker_definition.description.label,
                      name: worker_definition.description.queue_name,
                      parameters,
                      mode: StepMode::OneForOne,
                      multiple_jobs: None,
                      condition: None,
                      skip_destination_path: false,
                      parent_ids: vec![],
                      required_to_start: vec![],
                      work_dir: None,
                      jobs: None,
                      coordinates: Some(coord.clone()),
                    });

                    self.graph.add_node(index, coord);
                  }
                }
              }
            }
          }
        }
      }
      WorkflowGraphMessage::ToggleRequirementLinks(mouse_event) => {
        mouse_event.prevent_default();
        self.link_type = match self.link_type {
          LinkType::Parentage => LinkType::Requirement,
          LinkType::Requirement => LinkType::Parentage,
        };
      }
      WorkflowGraphMessage::OnMouseMove(mouse_event) => {
        mouse_event.prevent_default();

        match &self.moving {
          Some(Moving::View(moving_view)) => {
            self.offset.x = mouse_event.client_x() as isize - moving_view.x;
            self.offset.y = mouse_event.client_y() as isize - moving_view.y;
          }
          Some(Moving::Step(step_id, offset)) => {
            if is_definition {
              let coordinate = Coordinates {
                x: mouse_event.client_x() as isize - offset.x,
                y: mouse_event.client_y() as isize - offset.y,
              };
              self.graph.move_node(*step_id, coordinate);
            }
          }
          Some(Moving::Input(step_id, offset)) => {
            if is_definition {
              if let Some(node) = self.graph.get_node(*step_id) {
                let start = node.borrow().get_input_coordinates();

                self.drawing_link = Some(Link::new(
                  0,
                  0,
                  start,
                  Coordinates {
                    x: mouse_event.client_x() as isize - offset.x,
                    y: mouse_event.client_y() as isize - offset.y,
                  },
                  self.link_type,
                ))
              }
            }
          }
          Some(Moving::Output(step_id, offset)) => {
            if is_definition {
              if let Some(node) = self.graph.get_node(*step_id) {
                let end = node.borrow().get_output_coordinates();

                self.drawing_link = Some(Link::new(
                  0,
                  0,
                  Coordinates {
                    x: mouse_event.client_x() as isize - offset.x,
                    y: mouse_event.client_y() as isize - offset.y,
                  },
                  end,
                  self.link_type,
                ))
              }
            }
          }
          None => {}
        }
      }
      WorkflowGraphMessage::OnMouseUp(mouse_event) => {
        if let Some(Moving::Step(step_id, offset)) = &self.moving {
          if is_definition {
            let coordinate = Coordinates {
              x: mouse_event.client_x() as isize - offset.x,
              y: mouse_event.client_y() as isize - offset.y,
            };

            let mut workflow = ctx.props().workflow.lock().unwrap();

            if let Workflow::Definition(workflow_definition) = workflow.deref_mut() {
              if let Some(current_step) = workflow_definition.get_mut_step(*step_id) {
                current_step.coordinates = Some(coordinate.clone());
              }
            }
            self.graph.move_node(*step_id, coordinate);
          }
        }

        self.moving = None;
        self.drawing_link = None;
      }
      WorkflowGraphMessage::OnMouseUpStepInput(child_step_id) => {
        if let Some(Moving::Output(step_id, _offset)) = &self.moving {
          self.graph.connect(*step_id, child_step_id, &self.link_type);

          let mut workflow = ctx.props().workflow.lock().unwrap();

          if let Workflow::Definition(workflow_definition) = workflow.deref_mut() {
            if let Some(child_step) = workflow_definition.get_mut_step(child_step_id) {
              match self.link_type {
                LinkType::Parentage => child_step.add_parent(step_id),
                LinkType::Requirement => child_step.add_required(step_id),
              }
            }
          }
        }
        self.moving = None;
        self.drawing_link = None;
      }
      WorkflowGraphMessage::OnMouseUpStepOutput(parent_step_id) => {
        if let Some(Moving::Input(step_id, _offset)) = &self.moving {
          self
            .graph
            .connect(parent_step_id, *step_id, &self.link_type);

          let mut workflow = ctx.props().workflow.lock().unwrap();

          if let Workflow::Definition(workflow_definition) = workflow.deref_mut() {
            if let Some(child_step) = workflow_definition.get_mut_step(*step_id) {
              match self.link_type {
                LinkType::Parentage => child_step.add_parent(&parent_step_id),
                LinkType::Requirement => child_step.add_required(&parent_step_id),
              }
            }
          }
        }

        self.moving = None;
        self.drawing_link = None;
      }
      WorkflowGraphMessage::OnWheel(wheel_event) => {
        wheel_event.prevent_default();
        self.zoom = 0.01f32.max(self.zoom - wheel_event.delta_y() as f32 / 100.0);
      }
    }
    true
  }

  fn changed(&mut self, ctx: &Context<Self>) -> bool {
    self.graph = ctx
      .props()
      .workflow
      .lock()
      .unwrap()
      .to_graph(self.configuration.clone());

    ctx
      .props()
      .workflow
      .lock()
      .unwrap()
      .update_steps_coordinates_from_graph(&self.graph);
    true
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let mut links = self.graph.get_links(self.link_type);
    if let Some(link) = self.drawing_link.clone() {
      links.push(link);
    }

    let links: Html = links
          .iter()
          .map(|link| {
            let color = ctx
              .props()
              .selected_step
              .map(|id| {
                if id == link.start_node_id() {
                  COLOR_GREEN
                } else if id == link.end_node_id() {
                  COLOR_BLUE
                } else {
                  COLOR_GRAY
                }
              })
              .unwrap_or_else(|| COLOR_GRAY);

            let selected = ctx.props().selected_link.as_ref().map(|selected_link| selected_link == link).unwrap_or_default();

            html!(<SvgLink link={link.clone()} {color} callback={ctx.link().callback(|message| message)} {selected}></SvgLink>)
          })
          .collect();

    let is_definition = ctx.props().workflow.lock().unwrap().is_definition();

    let nodes: Html = ctx
      .props()
      .workflow
      .lock()
      .unwrap()
      .steps()
      .iter()
      .map(|step| {
        let selected = ctx.props().selected_step.map(|id| id == step.id).unwrap_or_default();

        self.graph.get_node(step.id).map(|node| {
          html!(<Node step={step.clone()} position={node.borrow().coordinates()} selected={selected} editable={is_definition} callback={ctx.link().callback(|message| message)}></Node>)
        }).unwrap_or_default()
      })
      .collect();

    let style = format!(
      "height: {}; width: {};",
      ctx.props().height,
      ctx.props().width,
    );

    let dataflow_style = format!(
      "transform: translate({}px, {}px) scale({});",
      self.offset.x, self.offset.y, self.zoom
    );

    let toggle_label = match self.link_type {
      LinkType::Parentage => "Parentage links",
      LinkType::Requirement => "Requirement links",
    };

    html!(
      <div class={self.style.clone()}
          onwheel={ctx.link().callback(WorkflowGraphMessage::OnWheel)}
          onmousedown={ctx.link().callback(WorkflowGraphMessage::OnMouseDown)}
          onmousemove={ctx.link().callback(WorkflowGraphMessage::OnMouseMove)}
          onmouseup={ctx.link().callback(WorkflowGraphMessage::OnMouseUp)}
          ondrop={ctx.link().callback(WorkflowGraphMessage::OnDrop)}
          ondragover={ctx.link().callback(WorkflowGraphMessage::OnDragOver)}
          onclick={ctx.link().callback(WorkflowGraphMessage::OnClick)}
          style={style}>
        <ToggleButton class="toggle"
          label={toggle_label}
          onclick={ctx.link().callback(WorkflowGraphMessage::ToggleRequirementLinks)}
          checked={self.link_type == LinkType::Requirement}/>
        <div class="drawflow" style={dataflow_style}
          >
          {links}
          {nodes}
        </div>
      </div>
    )
  }
}
