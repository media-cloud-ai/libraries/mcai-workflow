use crate::workflow_graph::workflow::WorkflowGraphMessage;
use css_in_rust_next::Style;
use mcai_graph::{Link, LinkType};
use web_sys::MouseEvent;
use yew::{classes, html, Callback, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct SvgLinkProperties {
  pub link: Link,
  pub color: String,
  #[prop_or(false)]
  pub selected: bool,
  pub callback: Callback<WorkflowGraphMessage>,
}

pub struct SvgLink {
  style: Style,
}

impl Component for SvgLink {
  type Message = ();
  type Properties = SvgLinkProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("svg_link.css")).unwrap();

    Self { style }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let start = &ctx.props().link.start();
    let end = &ctx.props().link.end();

    let path = format!(
      "M {} {} C {} {} {} {} {} {}",
      start.x,
      start.y,
      start.x,
      start.y - 40,
      end.x,
      end.y + 40,
      end.x,
      end.y
    );

    let width = match ctx.props().link.kind() {
      LinkType::Parentage => 3,
      LinkType::Requirement => 2,
    };
    let width = format!("{}px", width);
    let stroke = ctx.props().color.clone();
    let dashed = matches!(ctx.props().link.kind(), LinkType::Requirement).then(|| "6 4");

    let link = ctx.props().link.clone();
    let callback = ctx.props().callback.clone();
    let onclick = ctx.link().callback(move |event: MouseEvent| {
      event.stop_propagation();
      callback.emit(WorkflowGraphMessage::SelectLink(link.clone()));
    });

    let classes = ctx
      .props()
      .selected
      .then(|| classes!("path", "selected"))
      .unwrap_or_else(|| classes!("path"));

    html!(
      <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg" class={self.style.clone()}>
        <path class={classes}
          d={path}
          {stroke}
          stroke-width={width}
          fill="transparent"
          stroke-dasharray={dashed}
          {onclick}
          >
         </path>
      </svg>
    )
  }
}
