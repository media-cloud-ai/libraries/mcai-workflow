use super::workflow::WorkflowGraphMessage;
use crate::components::StepJobsProgress;
use css_in_rust_next::Style;
use mcai_models::Step;
use mcai_types::Coordinates;
use yew::{html, Callback, Component, Context, Html, MouseEvent, Properties};

#[derive(PartialEq, Properties)]
pub struct NodeProperties {
  pub step: Step,
  pub position: Coordinates,
  pub selected: bool,
  pub editable: bool,
  pub callback: Callback<WorkflowGraphMessage>,
}

#[derive(Debug, PartialEq)]
pub enum InternalMessage {
  Click(MouseEvent),
  Close(MouseEvent),
  OnMouseDown(MouseEvent),
  OnMouseDownInput(MouseEvent),
  OnMouseDownOutput(MouseEvent),
  OnMouseUpInput(MouseEvent),
  OnMouseUpOutput(MouseEvent),
}

pub struct Node {
  style: Style,
}

impl Component for Node {
  type Message = InternalMessage;
  type Properties = NodeProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("node.css")).unwrap();

    Node { style }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::Click(event) => {
        event.stop_propagation();

        ctx
          .props()
          .callback
          .emit(WorkflowGraphMessage::SelectStep(ctx.props().step.id));
      }
      InternalMessage::Close(event) => {
        event.prevent_default();

        ctx
          .props()
          .callback
          .emit(WorkflowGraphMessage::RemoveStep(ctx.props().step.id));
      }
      InternalMessage::OnMouseDown(mouse_event) => {
        mouse_event.prevent_default();

        let offset = Coordinates {
          x: mouse_event.client_x() as isize - ctx.props().position.x,
          y: mouse_event.client_y() as isize - ctx.props().position.y,
        };

        ctx
          .props()
          .callback
          .emit(WorkflowGraphMessage::SelectMovingStep(
            ctx.props().step.id,
            offset,
          ));
      }
      InternalMessage::OnMouseDownInput(mouse_event) => {
        let offset = Coordinates {
          x: mouse_event.client_x() as isize - ctx.props().position.x - 100 - 3,
          y: mouse_event.client_y() as isize - ctx.props().position.y - 3,
        };

        ctx
          .props()
          .callback
          .emit(WorkflowGraphMessage::SelectInputStep(
            ctx.props().step.id,
            offset,
          ));
      }
      InternalMessage::OnMouseDownOutput(mouse_event) => {
        let offset = Coordinates {
          x: mouse_event.client_x() as isize - ctx.props().position.x - 100 - 3,
          y: mouse_event.client_y() as isize - ctx.props().position.y - 50 - 3,
        };

        ctx
          .props()
          .callback
          .emit(WorkflowGraphMessage::SelectOutputStep(
            ctx.props().step.id,
            offset,
          ));
      }
      InternalMessage::OnMouseUpInput(_event) => {
        ctx
          .props()
          .callback
          .emit(WorkflowGraphMessage::OnMouseUpStepInput(
            ctx.props().step.id,
          ));
      }
      InternalMessage::OnMouseUpOutput(_event) => {
        ctx
          .props()
          .callback
          .emit(WorkflowGraphMessage::OnMouseUpStepOutput(
            ctx.props().step.id,
          ));
      }
    }
    false
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let style = format!(
      "left: {}px; top: {}px;",
      ctx.props().position.x,
      ctx.props().position.y
    );

    let classes = ctx.props().selected.then(|| "selected").unwrap_or_default();

    let progress = ctx
      .props()
      .step
      .jobs
      .clone()
      .map(|jobs_status| {
        html!(
          <StepJobsProgress jobs_status={jobs_status} expanded={false}>
          </StepJobsProgress>
        )
      })
      .unwrap_or_default();

    let icon = ctx
      .props()
      .step
      .icon
      .icon
      .as_ref()
      .map(|icon| {
        html!(
          <i class="material-icons">{icon}</i>
        )
      })
      .unwrap_or_default();

    html!(
      <slot class={self.style.clone()}>
        <div
          class={format!("node {}", classes)}
          style={style}
          >
          <div
            onclick={ctx.link().callback(InternalMessage::Click)}
            onmousedown={ctx.link().callback(InternalMessage::OnMouseDown)}>
            <div class="title">
              {icon}
              {&ctx.props().step.label}
            </div>
            {progress}
          </div>
          <div class="dot input"
            onmousedown={ctx.link().callback(InternalMessage::OnMouseDownInput)}
            onmouseup={ctx.link().callback(InternalMessage::OnMouseUpInput)}
            >
          </div>
          <div class="dot output"
            onmousedown={ctx.link().callback(InternalMessage::OnMouseDownOutput)}
            onmouseup={ctx.link().callback(InternalMessage::OnMouseUpOutput)}
            >
          </div>
          <i class="material-icons remove"
             title="Remove step"
             disabled={!ctx.props().editable}
             onclick={ctx.link().callback(InternalMessage::Close)}
             >
             {"close"}
          </i>
        </div>
      </slot>
    )
  }
}
