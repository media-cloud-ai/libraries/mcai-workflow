use crate::{
  Button, EditNotificationHook, EditNotificationHookMessage, EditStartParameter,
  EditStartParameterMessage, Field, McaiField, SharedWorkflow,
};
use css_in_rust_next::Style;
use mcai_graph::{Link, LinkType};
use mcai_models::{ParameterType, SchemaVersion, Workflow, WorkflowDefinition};
use std::ops::{Deref, DerefMut, Not};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{edit::Edit, plus::Plus, trash_2::Trash2};

#[derive(PartialEq, Properties)]
pub struct WorkflowPanelProperties {
  pub workflow: SharedWorkflow,
  pub height: String,
  pub width: String,
  pub step_id: Option<u32>,
  pub link: Option<Link>,
  pub event: Option<Callback<WorkflowPanelEvent>>,
}

pub enum WorkflowPanelEvent {
  SetStepIconEvent(u32),
  SetStepNameEvent(u32),
  RemovedLink(Link),
}

pub enum ModalStatus {
  Hidden,
  New,
  Edit(usize),
}

pub enum WorkflowPanelMessage {
  UpdateField,
  IconClick,
  StepNameClick,
  AddStartParameter,
  AddNotificationHook,
  AppendStartParameter(EditStartParameterMessage),
  AppendNotificationHook(EditNotificationHookMessage),
  EditStartParameter(usize),
  EditNotificationHook(usize),
  ToggleLive,
  RemoveStep(u32),
  RemoveLink(Link),
}

pub struct WorkflowPanel {
  style: Style,
  start_parameter_modal: ModalStatus,
  notification_hook_modal: ModalStatus,
}

impl Component for WorkflowPanel {
  type Message = WorkflowPanelMessage;
  type Properties = WorkflowPanelProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("panel.css")).unwrap();
    Self {
      style,
      start_parameter_modal: ModalStatus::Hidden,
      notification_hook_modal: ModalStatus::Hidden,
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      WorkflowPanelMessage::UpdateField => true,
      WorkflowPanelMessage::IconClick => {
        if let Some(callback) = ctx.props().event.as_ref() {
          if let Some(step_id) = ctx.props().step_id {
            callback.emit(WorkflowPanelEvent::SetStepIconEvent(step_id));
            return true;
          }
        }
        false
      }
      WorkflowPanelMessage::StepNameClick => {
        if let Some(callback) = ctx.props().event.as_ref() {
          if let Some(step_id) = ctx.props().step_id {
            callback.emit(WorkflowPanelEvent::SetStepNameEvent(step_id));
            return true;
          }
        }
        false
      }
      WorkflowPanelMessage::AddStartParameter => {
        self.start_parameter_modal = ModalStatus::New;
        true
      }
      WorkflowPanelMessage::AddNotificationHook => {
        self.notification_hook_modal = ModalStatus::New;
        true
      }

      WorkflowPanelMessage::AppendStartParameter(edit_start_message) => {
        match edit_start_message {
          EditStartParameterMessage::Submit(start_parameter) => {
            if let Workflow::Definition(workflow_definition) =
              ctx.props().workflow.lock().unwrap().deref_mut()
            {
              if let ModalStatus::Edit(index) = self.start_parameter_modal {
                if let Some(parameter) = workflow_definition
                  .get_mut_start_parameters()
                  .get_mut(index)
                {
                  *parameter = start_parameter;
                }
              } else {
                workflow_definition
                  .get_mut_start_parameters()
                  .push(start_parameter);
              }

              self.start_parameter_modal = ModalStatus::Hidden;
            } else {
              return false;
            }
          }
          EditStartParameterMessage::Cancel => {
            self.start_parameter_modal = ModalStatus::Hidden;
          }
          EditStartParameterMessage::Delete => {
            if let ModalStatus::Edit(index) = self.start_parameter_modal {
              if let Workflow::Definition(workflow_definition) =
                ctx.props().workflow.lock().unwrap().deref_mut()
              {
                workflow_definition.get_mut_start_parameters().remove(index);
              }
            }
            self.start_parameter_modal = ModalStatus::Hidden;
          }
        }
        true
      }
      WorkflowPanelMessage::AppendNotificationHook(edit_notification_hook) => {
        match edit_notification_hook {
          EditNotificationHookMessage::Submit(notification_hook) => {
            if let Workflow::Definition(workflow_definition) =
              ctx.props().workflow.lock().unwrap().deref_mut()
            {
              if let ModalStatus::Edit(index) = self.notification_hook_modal {
                if let Some(notification_hooks) = workflow_definition.get_mut_notification_hooks() {
                  if let Some(parameter) = notification_hooks.get_mut(index) {
                    *parameter = notification_hook;
                  }
                }
              } else if let Some(notification_hooks) =
                workflow_definition.get_mut_notification_hooks()
              {
                notification_hooks.push(notification_hook)
              }

              self.notification_hook_modal = ModalStatus::Hidden;
              true
            } else {
              false
            }
          }
          EditNotificationHookMessage::Cancel => {
            self.notification_hook_modal = ModalStatus::Hidden;
            true
          }
          EditNotificationHookMessage::Delete => {
            if let ModalStatus::Edit(index) = self.notification_hook_modal {
              if let Workflow::Definition(workflow_definition) =
                ctx.props().workflow.lock().unwrap().deref_mut()
              {
                workflow_definition
                  .get_mut_notification_hooks()
                  .map(|notification_hooks| notification_hooks.remove(index));
              }
            }
            self.notification_hook_modal = ModalStatus::Hidden;
            true
          }
        }
      }
      WorkflowPanelMessage::EditStartParameter(index) => {
        self.start_parameter_modal = ModalStatus::Edit(index);
        true
      }
      WorkflowPanelMessage::EditNotificationHook(index) => {
        self.notification_hook_modal = ModalStatus::Edit(index);
        true
      }
      WorkflowPanelMessage::ToggleLive => {
        if let Workflow::Definition(definition) = ctx.props().workflow.lock().unwrap().deref_mut() {
          definition.toggle_is_live()
        }
        true
      }
      WorkflowPanelMessage::RemoveStep(step_id) => {
        let mut workflow = ctx.props().workflow.lock().unwrap();

        if let Workflow::Definition(workflow_definition) = workflow.deref_mut() {
          let steps = workflow_definition.get_mut_steps();

          steps.retain(|step| step.id != step_id);

          steps.iter_mut().for_each(|step| {
            step.parent_ids.retain(|parent_id| *parent_id != step_id);
            step
              .required_to_start
              .retain(|required| *required != step_id);
          });
        }

        true
      }
      WorkflowPanelMessage::RemoveLink(link) => {
        let mut workflow = ctx.props().workflow.lock().unwrap();

        if let Workflow::Definition(workflow_definition) = workflow.deref_mut() {
          if let Some(step) = workflow_definition.get_mut_step(link.start_node_id()) {
            let parents = match link.kind() {
              LinkType::Parentage => &mut step.parent_ids,
              LinkType::Requirement => &mut step.required_to_start,
            };
            parents.retain(|id| *id != link.end_node_id());
          }
        }

        if let Some(callback) = ctx.props().event.as_ref() {
          callback.emit(WorkflowPanelEvent::RemovedLink(link));
        }

        true
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let style = format!(
      "height: {}; width: {};",
      ctx.props().height,
      ctx.props().width,
    );

    let modal_edit_start_parameter = match self.start_parameter_modal {
      ModalStatus::Hidden => html!(),
      ModalStatus::New => {
        html!(<EditStartParameter event={ctx.link().callback(WorkflowPanelMessage::AppendStartParameter)} />)
      }
      ModalStatus::Edit(index) => {
        let start_parameter = if let Workflow::Definition(workflow_definition) =
          ctx.props().workflow.lock().unwrap().deref()
        {
          workflow_definition
            .get_start_parameters()
            .get(index)
            .cloned()
        } else {
          None
        };

        html!(
          <EditStartParameter event={ctx.link().callback(WorkflowPanelMessage::AppendStartParameter)} start_parameter={start_parameter} />
        )
      }
    };

    let modal_notification_hook = match self.notification_hook_modal {
      ModalStatus::Hidden => html!(),
      ModalStatus::New => {
        html!(<EditNotificationHook event={ctx.link().callback(WorkflowPanelMessage::AppendNotificationHook)} />)
      }
      ModalStatus::Edit(index) => {
        let notification_hook = if let Workflow::Definition(workflow_definition) =
          ctx.props().workflow.lock().unwrap().deref()
        {
          if let Some(notification_hooks) = workflow_definition.get_notification_hooks() {
            notification_hooks.get(index).cloned()
          } else {
            None
          }
        } else {
          None
        };

        html!(<EditNotificationHook event={ctx.link().callback(WorkflowPanelMessage::AppendNotificationHook)} notification_hook={notification_hook} />)
      }
    };

    let is_definition = ctx.props().workflow.lock().unwrap().is_definition();
    let schema_version = ctx.props().workflow.lock().unwrap().schema_version();

    let start_parameters: Html = ctx.props().workflow.lock().unwrap()
      .get_start_parameters()
      .iter()
      .enumerate()
      .map(|(index, start_parameter)| {
        let edit_button = is_definition.then(|| html!(
          <span class="edit_start_parameter">
            <button onclick={ctx.link().callback(move |_| WorkflowPanelMessage::EditStartParameter(index))}>
              <Edit />
            </button>
          </span>
        )).unwrap_or_default();

        html!(
          <div class="start_parameter">
            <label>
              {&start_parameter.label}
            </label>
            {edit_button}
          </div>
        )
      })
      .collect();

    let add_start_parameter = is_definition
      .then(|| {
        html!(
          <div class="actions">
            <Button
              label="Add start parameter"
              icon={html!(<Plus />)}
              onclick={ctx.link().callback(|_| WorkflowPanelMessage::AddStartParameter)}
              />
          </div>
        )
      })
      .unwrap_or_default();

    let start_parameters = html!(
      <>
        <div>
          <label class="sub_title">
            {"Start parameters"}
          </label>
        </div>
        {start_parameters}
        {add_start_parameter}
      </>
    );

    let notification_hooks: Html = if let Some(notification_hooks) = ctx
      .props()
      .workflow
      .lock()
      .unwrap()
      .get_notification_hooks()
    {
      notification_hooks
        .iter()
        .enumerate()
        .map(|(index, notification_hook)| {
          let edit_button = is_definition.then(|| html!(
            <span class="edit_notification_hook">
              <button onclick={ctx.link().callback(move |_| WorkflowPanelMessage::EditNotificationHook(index))}>
                <Edit />
              </button>
            </span>
          )).unwrap_or_default();

          html!(
            <div class="notification_hook">
              <label>
                {&notification_hook.label}
              </label>
              {edit_button}
            </div>
          )
        })
        .collect()
    } else {
      html!()
    };

    let add_notification_hook = is_definition
      .then(|| {
        html!(
          <div class="actions">
            <Button
              label="Add notification hook"
              icon={html!(<Plus />)}
              onclick={ctx.link().callback(|_| WorkflowPanelMessage::AddNotificationHook)}
              />
          </div>
        )
      })
      .unwrap_or_default();

    let notifications_hooks = if schema_version == SchemaVersion::_1_11 {
      html!(
        <>
          <div>
            <label class="sub_title">
              {"Notification hooks"}
            </label>
          </div>
          {notification_hooks}
          {add_notification_hook}
        </>
      )
    } else {
      html!()
    };

    let step_information = ctx
      .props()
      .step_id
      .map(|step_id| {
        let workflow = ctx
          .props()
          .workflow
          .lock()
          .unwrap();

        let jobs: Vec<Html> = workflow
          .jobs()
          .map(|jobs|
            jobs
              .iter()
              .filter(|job| job.step_id == step_id)
              .map(|job| {
                let parameters: Html = job.parameters
                    .iter()
                    .map(|parameter| {
                      html!(
                        <Field label={parameter.id.content.clone()}>
                          <McaiField
                            kind={parameter.kind.clone()}
                            step_id={Some(step_id)}
                            field_name={parameter.id.content.clone()}
                            workflow={ctx.props().workflow.clone()}
                            event={ctx.link().callback(|_| WorkflowPanelMessage::UpdateField)}
                            />
                        </Field>
                      )
                    }).collect();

                let progression = job.get_last_progression();

                let classes = format!("job {}", job.status.first().map(|job_status| job_status.state.clone()).unwrap_or_default());

                html!(
                  <div class={classes}>
                    <Field label="Title">
                      {&job.name}
                    </Field>
                    <Field label="Status">
                      <span class="badge status">
                        {job.status.first().map(|job_status| html!({&job_status.state})).unwrap_or_default()}
                      </span>
                    </Field>
                    <Field label="Last Worker Instance ID">
                      {job.last_worker_instance_id.clone().unwrap_or_default()}
                    </Field>
                    <Field label="Progression">
                      {progression.map(|progression| progression.progression).unwrap_or_default()}
                    </Field>
                    <div>
                      <label class="sub_title">{"Parameters"}</label>
                      <span>
                        {parameters}
                      </span>
                    </div>
                  </div>
                )
              })
              .collect()
          )
          .unwrap_or_default();

        let jobs = jobs
          .is_empty()
          .not()
          .then(||
            html!(
              <div class="jobs">
                <label class="sub_title">{"Jobs"}</label>
                <div class="inner">
                  {jobs}
                </div>
              </div>
            )
          )
          .unwrap_or_default();

        workflow
          .steps()
          .iter()
          .find(|step| step.id == step_id)
          .map(|step| {
            let parameters: Html = step.parameters
              .iter()
              .map(|parameter| {
                html!(
                  <div class="field parameter">
                    <label>
                      {&parameter.id.content}
                    </label>
                    <McaiField
                      kind={parameter.kind.clone()}
                      step_id={Some(step_id)}
                      field_name={parameter.id.content.clone()}
                      workflow={ctx.props().workflow.clone()}
                      event={ctx.link().callback(|_| WorkflowPanelMessage::UpdateField)}
                      />
                  </div>
                )
              }).collect();

            let step_name = ParameterType::String {default: None, value: Some(step.name.clone()), required: true };
            let step_icon = ParameterType::String {default: None, value: Some(step.icon.to_string()), required: false };
            let step_id = step.id;

            html! {
              <div class="step">
                <div class="title">
                  {format!("Step {}", step.label)}
                </div>
                <div class="content">
                  <Field label="Queue name">
                    <McaiField
                      kind={step_name}
                      step_id={step.id}
                      field_name="name"
                      workflow={ctx.props().workflow.clone()}
                      event={ctx.link().callback(|_| WorkflowPanelMessage::UpdateField)}
                      />
                  </Field>

                  <Field label="Icon">
                    <McaiField
                      kind={step_icon}
                      step_id={step.id}
                      field_name="icon"
                      is_icon=true
                      workflow={ctx.props().workflow.clone()}
                      event={ctx.link().callback(|_| WorkflowPanelMessage::UpdateField)}
                      />
                  </Field>
                  <div>
                    <label class="sub_title">{"Parameters"}</label>
                    <span>
                      {parameters}
                    </span>
                  </div>
                  {jobs}
                </div>
                <div class="footer">
                  <Button
                    label={"Remove this step"}
                    icon={html!(<Trash2 />)}
                    disabled={!is_definition}
                    onclick={ctx.link().callback(move |_| WorkflowPanelMessage::RemoveStep(step_id))}
                    />
                </div>
              </div>
            }
          })
          .unwrap_or_default()
      })
      .unwrap_or_else(|| {
        let scope = ctx.link();
        let steps = ctx.props().workflow.lock().unwrap().steps().clone();

        if let Some(html) = ctx.props().link.clone().map(|link| {
          let start_step = steps.iter().find(|s| s.id == link.start_node_id());
          let end_step = steps.iter().find(|s| s.id == link.end_node_id());

          let title = format!("{} --> {}", start_step.map(|s| s.label.clone()).unwrap_or_default(), end_step.map(|s| s.label.clone()).unwrap_or_default());
          let link_type = match  link.kind() {
            LinkType::Parentage => "Parentage",
            LinkType::Requirement => "Requirement"
          };

          html!(
            <div class="link">
              <div class="title">
                {title}
              </div>
              <div class="content">
                <Field label="Link type">{link_type}</Field>
              </div>
              <div class="footer">
                <Button
                  label={"Remove this link"}
                  icon={html!(<Trash2 />)}
                  disabled={!is_definition}
                  onclick={scope.callback(move |_| WorkflowPanelMessage::RemoveLink(link.clone()))}
                  />
              </div>
            </div>
          )
        }) {
          return html;
        }

        let reference = ctx
          .props()
          .workflow
          .lock()
          .unwrap()
          .reference()
          .map(|reference| html!(<Field label="Reference">{reference}</Field>))
          .unwrap_or_default();

        let workflow = ctx.props().workflow.lock().unwrap();
        let workflow_identifier = workflow.identifier();
        let workflow_label = workflow.label();
        let version = workflow.version();
        let is_live = workflow.is_live();

        let schema_version = match workflow.deref() {
          Workflow::Definition(WorkflowDefinition::Version1_8(_)) => "1.8".to_string(),
          Workflow::Definition(WorkflowDefinition::Version1_9(_)) => "1.9".to_string(),
          Workflow::Definition(WorkflowDefinition::Version1_10(_)) => "1.10".to_string(),
          Workflow::Definition(WorkflowDefinition::Version1_11(_)) => "1.11".to_string(),
          Workflow::Instance(workflow_instance) => workflow_instance.schema_version.clone()
        };

        let inner_identifier = if workflow.is_definition() {
          html!(
            <McaiField
              kind={ParameterType::String{value: Some(workflow_identifier.to_string()), default: None, required: true}}
              field_name={"identifier"}
              workflow={ctx.props().workflow.clone()}
              event={ctx.link().callback(|_| WorkflowPanelMessage::UpdateField)}
              />
          )
        } else {
          html!({workflow_identifier})
        };

        let inner_label = if workflow.is_definition() {
          html!(
            <McaiField
              kind={ParameterType::String{value: Some(workflow_label.to_string()), default: None, required: true}}
              field_name={"label"}
              workflow={ctx.props().workflow.clone()}
              event={ctx.link().callback(|_| WorkflowPanelMessage::UpdateField)}
              />
          )
        } else {
          html!({workflow_label})
        };

        let inner_tags = if workflow.is_definition() {
          html!(
            <McaiField
              kind={ParameterType::ArrayOfStrings{value: workflow.tags().to_vec(), default: vec![], required: false}}
              field_name={"tags"}
              workflow={ctx.props().workflow.clone()}
              event={ctx.link().callback(|_| WorkflowPanelMessage::UpdateField)}
              />
          )
        } else {
          workflow.tags().iter().map(|tag| html!(<span class="tag">{tag}</span>)).collect::<Html>()
        };

        html!(
          <>
            <div class="title">
              <label>{format!("Workflow {}", workflow_label)}</label>
            </div>
            <div class="content">
              <Field label="Identifier">{inner_identifier}</Field>
              <Field label="Label">{inner_label}</Field>
              <Field label="Version">{version}</Field>
              <Field label="Schema version">{schema_version}</Field>
              <Field label="Is live">
                <input type="checkbox" checked={is_live} disabled={!workflow.is_definition()} onclick={ctx.link().callback(|_| WorkflowPanelMessage::ToggleLive)} />
              </Field>
              <Field label="Tags">{inner_tags}</Field>
              {reference}
              {start_parameters}
              {notifications_hooks}
            </div>
          </>
        )
      });

    html!(
      <span class={self.style.clone()} style={style}>
        {step_information}
        {modal_edit_start_parameter}
        {modal_notification_hook}
      </span>
    )
  }
}
