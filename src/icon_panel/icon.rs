use css_in_rust_next::Style;
use heck::AsTitleCase;
use material_icons::icon_to_html_name;
use material_icons::Icon as MaterialIcon;
use yew::{html, Callback, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct IconProperties {
  pub material_icon: MaterialIcon,
  pub event: Callback<IconEvent>,
}

pub enum IconEvent {
  SelectedIcon(String),
}

#[derive(Debug)]
pub enum IconMessage {
  Icon,
}

pub struct Icon {
  style: Style,
}

impl Component for Icon {
  type Message = IconMessage;
  type Properties = IconProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("icon.css")).unwrap();
    Self { style }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    if matches!(msg, IconMessage::Icon) {
      ctx.props().event.emit(IconEvent::SelectedIcon(
        icon_to_html_name(&ctx.props().material_icon).to_string(),
      ))
    }
    false
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let icon_html_name = icon_to_html_name(&ctx.props().material_icon);
    let label = AsTitleCase(icon_html_name);

    html!(
      <div class={self.style.clone()}>
        <div onclick={ctx.link().callback(|_|IconMessage::Icon)} class="icon">
          <i class="material-icons">{icon_html_name}</i>
          <label>{label}</label>
        </div>
      </div>
    )
  }
}
