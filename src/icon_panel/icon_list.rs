use crate::{Icon, IconEvent};
use css_in_rust_next::Style;
use enum_iterator::IntoEnumIterator;
use material_icons::{icon_to_html_name, Icon as MaterialIcon};
use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlInputElement, InputEvent};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{chevrons_left::ChevronsLeft, chevrons_right::ChevronsRight};

#[derive(PartialEq, Properties)]
pub struct IconListProperties {
  pub event: Option<Callback<IconEvent>>,
}

pub enum IconListMessage {
  Icon(IconEvent),
  NextPage,
  PreviousPage,
  Search(String),
}

pub struct IconList {
  style: Style,
  material_icons: Vec<MaterialIcon>,
  page: usize,
  search_value: Option<String>,
}

impl Component for IconList {
  type Message = IconListMessage;
  type Properties = IconListProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("icon_list.css")).unwrap();

    let material_icons: Vec<MaterialIcon> = MaterialIcon::into_enum_iter().collect();

    Self {
      style,
      material_icons,
      page: 0,
      search_value: None,
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      IconListMessage::Icon(event) => {
        if let Some(callback) = ctx.props().event.as_ref() {
          callback.emit(event)
        }
        false
      }
      IconListMessage::PreviousPage => {
        self.page > 0 && {
          self.page -= 1;
          true
        }
      }
      IconListMessage::NextPage => {
        self.page < self.material_icons.len() - 1 && {
          self.page += 1;
          true
        }
      }
      IconListMessage::Search(value) => {
        self.search_value = Some(value);
        true
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let mut material_icons_iterator = self.material_icons.iter();

    let number_of_icons_per_page = 20;

    let icons: Html = material_icons_iterator
      .by_ref()
      .filter(|icon| {
        self.search_value.is_none()
          || self
            .search_value
            .as_ref()
            .map(|search_value| icon_to_html_name(icon).contains(search_value.as_str()))
            .unwrap_or_default()
      })
      .skip(self.page * number_of_icons_per_page)
      .take(number_of_icons_per_page)
      .map(|material_icon| {
        html!(
          <Icon event={ctx.link().callback(IconListMessage::Icon)} material_icon={*material_icon} />
        )
      })
      .collect();

    let search_callback = ctx.link().batch_callback(|e: InputEvent| {
      let target: Option<EventTarget> = e.target();
      let input = target.and_then(|t| t.dyn_into::<HtmlInputElement>().ok());
      input.map(|input| IconListMessage::Search(input.value()))
    });

    html!(
      <div class={self.style.clone()}>
        <div class="searchBar">
          <input placeholder="Search" oninput={search_callback}/>
        </div>
        <div class="icon_list">
          {icons}
        </div>
        <div class="controls">
          <button class="pagination" onclick={ctx.link().callback(|_|IconListMessage::PreviousPage)} disabled={self.page == 0}>
            <ChevronsLeft />
            {"Previous"}
          </button>
          <button class="pagination" onclick={ctx.link().callback(|_|IconListMessage::NextPage)} disabled={material_icons_iterator.next().is_none()}>
            <ChevronsRight />
            {"Next"}
          </button>
        </div>
      </div>
    )
  }
}
