use crate::components::StepJobsProgress;
use css_in_rust_next::Style;
use mcai_models::WorkflowInstance;
use yew::{html, Component, Context, Html, Properties};
use yew_feather::{chevron_down::ChevronDown, chevron_up::ChevronUp};

#[derive(PartialEq, Properties)]
pub struct WorkflowLineProperties {
  pub workflow: WorkflowInstance,
  pub duration: Option<usize>,
}

#[derive(PartialEq)]
pub enum InternalMessage {
  ToggleExpand,
}

pub struct WorkflowLine {
  style: Style,
  expanded: bool,
}

impl Component for WorkflowLine {
  type Message = InternalMessage;
  type Properties = WorkflowLineProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("workflow_line.css")).unwrap();

    WorkflowLine {
      style,
      expanded: false,
    }
  }

  fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
    if msg == InternalMessage::ToggleExpand {
      self.expanded = !self.expanded
    }
    true
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let workflow = &ctx.props().workflow;

    let created_at = workflow
      .created_at
      .clone()
      .format("%e %B %Y %H:%M:%S")
      .to_string();

    let duration = ctx
      .props()
      .duration
      .map(|duration| {
        let hours = duration / 60 / 60;
        let minutes = (duration / 60) % 60;
        let seconds = duration % 60;

        format!("{:02}:{:02}:{:02}", hours, minutes, seconds)
      })
      .unwrap_or_else(|| "00:00:00".to_string());

    let icon = if self.expanded {
      html!(<ChevronUp></ChevronUp>)
    } else {
      html!(<ChevronDown></ChevronDown>)
    };

    let steps: Html = workflow
      .steps
      .iter()
      .map(|step| {
        step
          .jobs
          .clone()
          .map(|jobs_status|
            html!(
              <StepJobsProgress jobs_status={jobs_status} expanded={self.expanded} title={step.label.clone()}></StepJobsProgress>
            )
          ).unwrap_or_default()
      })
      .collect();

    html!(
      <div class={self.style.clone()}>
        <div>
          <span class="icon" onclick={ctx.link().callback(|_| {InternalMessage::ToggleExpand})}>
            {icon}
          </span>
          <span class="id">
            {"#"}{workflow.id}
          </span>
          <span>
            {&workflow.identifier}
          </span>
          <span>
            {"Created at: "}{created_at}
          </span>
          <span>
            {"Duration: "}{duration}
          </span>
          <span>
            {"Steps: "}{workflow.get_passed_steps_count()}{"/"}{workflow.steps.len()}
          </span>
        </div>
        <div>
          {steps}
        </div>
      </div>
    )
  }
}
