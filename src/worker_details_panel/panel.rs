use crate::SharedWorkerDefinition;
use css_in_rust_next::Style;
use schemars::schema::{InstanceType, Schema, SchemaObject, SingleOrVec};
use web_sys::Node;
use yew::virtual_dom::VNode;
use yew::{html, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct WorkerDetailsPanelProperties {
  pub height: String,
  pub width: String,
  pub worker_definition: SharedWorkerDefinition,
}

pub struct WorkerDetailsPanel {
  style: Style,
}

impl Component for WorkerDetailsPanel {
  type Message = ();
  type Properties = WorkerDetailsPanelProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("panel.css")).unwrap();
    Self { style }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let style = format!(
      "height: {}; width: {};",
      ctx.props().height,
      ctx.props().width,
    );

    let worker = ctx.props().worker_definition.lock().unwrap();

    let description = web_sys::window()
      .unwrap()
      .document()
      .unwrap()
      .create_element("div")
      .unwrap();

    description.set_inner_html(&worker.description.description.replace('\n', "<br/>"));

    let description = VNode::VRef(Node::from(description));

    let parameters: Html = worker
      .parameters
      .schema
      .object
      .as_ref()
      .unwrap()
      .properties
      .iter()
      .map(|(key, kind)| {
        let (parameter_title, parameter_type) = match kind {
          Schema::Object(SchemaObject {
            metadata,
            instance_type: Some(SingleOrVec::Single(instance_type)),
            ..
          }) => {
            let kind = match *instance_type.as_ref() {
              InstanceType::String => "String",
              InstanceType::Boolean => "Boolean",
              InstanceType::Array => "Array",
              InstanceType::Integer => "Integer",
              InstanceType::Number => "Number",
              InstanceType::Object => "Object",
              InstanceType::Null => "Null",
            };

            let title: String = metadata
              .as_ref()
              .and_then(|metadata| metadata.title.clone())
              .unwrap_or_default();

            (html!(title), html!(kind))
          }
          Schema::Object(SchemaObject {
            metadata,
            instance_type: Some(SingleOrVec::Vec(instance_type)),
            ..
          }) => {
            let mut kinds = instance_type
              .iter()
              .filter(|kind| kind != &&InstanceType::Null)
              .map(|kind| match *kind {
                InstanceType::String => "String",
                InstanceType::Boolean => "Boolean",
                InstanceType::Array => "Array",
                InstanceType::Integer => "Integer",
                InstanceType::Number => "Number",
                InstanceType::Object => "Object",
                InstanceType::Null => "Null",
              })
              .collect::<Vec<&str>>()
              .join(", ");

            if instance_type.contains(&InstanceType::Null) {
              kinds += " (optional)";
            }

            let title: String = metadata
              .as_ref()
              .and_then(|metadata| metadata.title.clone())
              .unwrap_or_default();

            (html!(title), html!({ kinds }))
          }
          _ => (html!(""), html!({ "Complex type" })),
        };

        html!(
          <div class="field">
            <label>
              {key}
              <span class="type">
                {parameter_type}
              </span>
            </label>
            <span class="description">
              {parameter_title}
            </span>
          </div>
        )
      })
      .collect();

    html!(
      <span class={self.style.clone()} style={style}>
        <div class="title">
          <label>{format!("Worker {}", worker.description.label)}</label>
        </div>

        <div class="content">
          <div class="field">
            <label>
              {"Short description"}
            </label>
            <span>
              <div class="description">
                {{&worker.description.short_description}}
              </div>
            </span>
          </div>
          <div class="field">
            <label>
              {"Description"}
            </label>
            <span>
              <div class="description">
                {description}
              </div>
            </span>
          </div>
          <div class="field">
            <label>
              {"Version"}
            </label>
            <span>
              {{&worker.description.version}}
            </span>
          </div>
          <div class="field">
            <label>
              {"SDK Version"}
            </label>
            <span>
              {{&worker.description.sdk_version}}
            </span>
          </div>
          <h2>{"Parameters"}</h2>
          {parameters}
        </div>
      </span>
    )
  }
}
