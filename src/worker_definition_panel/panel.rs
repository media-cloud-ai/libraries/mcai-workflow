use super::WorkerInformation;
use by_address::ByAddress;
use css_in_rust_next::Style;
use mcai_models::WorkerDefinition;
use std::sync::{Arc, Mutex};
use yew::{html, Callback, Component, Context, Html, Properties};

pub type SharedWorkerDefinition = ByAddress<Arc<Mutex<WorkerDefinition>>>;

#[derive(PartialEq, Properties)]
pub struct WorkerDefinitionPanelProperties {
  pub height: String,
  pub width: String,
  pub worker_definitions: Vec<SharedWorkerDefinition>,
  pub selected_worker_definition: Option<SharedWorkerDefinition>,
  pub events: Option<Callback<WorkerDefinitionEvent>>,
}

pub enum WorkerDefinitionEvent {
  WorkerDeselected,
  WorkerSelected(SharedWorkerDefinition),
}

pub enum WorkerDefinitionMessage {
  WorkerSelected(SharedWorkerDefinition),
}

pub struct WorkerDefinitionPanel {
  style: Style,
}

impl Component for WorkerDefinitionPanel {
  type Message = WorkerDefinitionMessage;
  type Properties = WorkerDefinitionPanelProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("panel.css")).unwrap();
    Self { style }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      WorkerDefinitionMessage::WorkerSelected(worker_definition) => {
        let event = if ctx.props().selected_worker_definition == Some(worker_definition.clone()) {
          WorkerDefinitionEvent::WorkerDeselected
        } else {
          WorkerDefinitionEvent::WorkerSelected(worker_definition)
        };

        if let Some(callback) = ctx.props().events.as_ref() {
          callback.emit(event)
        }
      }
    }
    false
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let style = format!(
      "height: {}; width: {};",
      ctx.props().height,
      ctx.props().width,
    );

    let list = ctx.props().worker_definitions.clone();

    let worker_list: Html = list
      .iter()
      .map(|worker| {
        let worker_definition = worker.clone();

        let selected = ctx
          .props()
          .selected_worker_definition
          .as_ref()
          .map(|worker| worker_definition == *worker)
          .unwrap_or_default();

        html!(
          <WorkerInformation
            worker={worker.clone()}
            events={ctx.link().callback(move |_| WorkerDefinitionMessage::WorkerSelected(worker_definition.clone()))}
            selected={selected}
            />
        )
      })
      .collect();

    html!(
      <span class={self.style.clone()} style={style}>
        <div class="title">
          <label>{"Workers"}</label>
        </div>

        <div class="content">
          {worker_list}
        </div>
      </span>
    )
  }
}
