use super::SharedWorkerDefinition;
use crate::{DragAndDropMessage, MCAI_DRAG_AND_DROP_ID};
use css_in_rust_next::Style;
use web_sys::{DragEvent, MouseEvent};
use yew::{html, Callback, Component, Context, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct WorkerInformationProperties {
  pub worker: SharedWorkerDefinition,
  pub selected: bool,
  pub events: Callback<()>,
}

#[derive(Debug)]
pub enum WorkerInformationMessage {
  OnClick(MouseEvent),
  OnDragStart(DragEvent),
}

pub struct WorkerInformation {
  style: Style,
}

impl Component for WorkerInformation {
  type Message = WorkerInformationMessage;
  type Properties = WorkerInformationProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("worker_information.css")).unwrap();
    Self { style }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      WorkerInformationMessage::OnClick(_event) => {
        ctx.props().events.emit(());
      }
      WorkerInformationMessage::OnDragStart(event) => {
        if let Some(data_transfer) = event.data_transfer() {
          let data = DragAndDropMessage::Worker(ctx.props().worker.lock().unwrap().clone());

          data_transfer
            .set_data(
              MCAI_DRAG_AND_DROP_ID,
              &serde_json::to_string(&data).unwrap(),
            )
            .unwrap();
        }
      }
    }
    true
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let worker = ctx.props().worker.lock().unwrap().clone();

    let classes = ctx.props().selected.then(|| "selected").unwrap_or_default();

    html!(
      <slot class={self.style.clone()}>
        <div class={format!("worker {}", classes)}
          draggable="true"
          onclick={ctx.link().callback(WorkerInformationMessage::OnClick)}
          ondragstart={ctx.link().callback(WorkerInformationMessage::OnDragStart)}
          >
          <label>{worker.description.label}</label>
          <span>{worker.description.version}</span>
        </div>
      </slot>
    )
  }
}
